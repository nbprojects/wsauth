<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * 
 */
class DefaultController extends Controller
{
    
    /**
     * @Route("/demo")
     */
    public function demoAction()
    {        
        $rendered = $this->renderView( 'AppBundle:demo:index.html.twig');
        $response = new Response( $rendered );        

        return $response;
    }
    
}
