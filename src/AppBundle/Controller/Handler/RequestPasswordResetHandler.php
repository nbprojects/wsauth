<?php

namespace AppBundle\Controller\Handler;

use GuzzleHttp\Client;
use AppBundle\Services\Token\TokenManager;
use Texyon\Database\PortalBundle\Entity\Repository\AccountsRepository;
use Texyon\Database\PortalBundle\Entity\Accounts;


/**
 * Description of RequestPasswordResetHandler
 *
 */
class RequestPasswordResetHandler
{
    /** @var AccountsRepository */
    private $accountsRepository;
    /** @var  TokenManager */
    private $tokenManager;
    /** @var Client */    
    private $wsEmailClient;
        
    
    /**
     * 
     * @param AccountsRepository $accountsRepository
     * @param TokenManager $tokenManager
     * @param Client $wsEmailClient
     */
    public function __construct(
        AccountsRepository $accountsRepository,
        TokenManager $tokenManager,
        Client $wsEmailClient
    ) {
        $this->accountsRepository = $accountsRepository;
        $this->tokenManager = $tokenManager;
        $this->wsEmailClient = $wsEmailClient;
    }
    
    /**
     * 
     * @param Accounts $account
     * @param string $locale
     * @param string $destination
     * @param string $ipAddress
     * @return array     
     */
    public function handle(Accounts $account, $locale, $destination, $ipAddress)
    {
        $token = $this->tokenManager->createChangePasswordToken($account, $ipAddress);
        $this->sendEmail($account->getEmail(), $locale, $destination, $token);
        
        return true;
    }
    
    private function sendEmail($email, $locale, $destination, $token)
    {
        //$urlPasswordForgot = sprintf('%s?action=rp&token=%s', $referer, $token);
        $urlPasswordForgot = sprintf('%s/%s/#validate:%s', $destination, $locale, $token); // referer = "http://www.texyon.com"

        return $this->wsEmailClient->post('send', ['form_params' => [
                'type' => 'forgot_password',
                'email' => $email,
                'locale' => $locale,
                'data' => ['-token-' => $token, '-url-' => $urlPasswordForgot]
            ]            
        ]); 
    }
        
}
