<?php

namespace AppBundle\Controller\Handler;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Description of LoginHandler
 *
 */
class LoginHandler
{
    /** @var \GuzzleHttp\Client  */
    private $clientOauth;
        
    
    public function __construct(
        Client $clientOauth
    ) {
        $this->clientOauth = $clientOauth;
    }
    
    /**
     * 
     * @param string $username
     * @param string $password
     */
    public function handle($username, $password)
    {
        try{
            $config = $this->clientOauth->getConfig();
   
            $options = [
                'query' => array_merge(
                    $config['query'],
                    [
                        'username' => $username,
                        'password' => $password,
                        'grant_type' => 'password',
                        'scope' => 'user',
                    ])
            ];
   
            $response = $this->clientOauth->get('token', $options);

            return json_decode($response->getBody(), true);
        }catch (ClientException $ex) {
            $client_ex = $ex->getResponseBodySummary( $ex->getResponse() );            
            throw new BadRequestHttpException( $client_ex, $ex);
        }
    }
}
