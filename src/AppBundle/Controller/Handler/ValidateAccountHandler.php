<?php

namespace AppBundle\Controller\Handler;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Texyon\Database\PortalBundle\Entity\Repository\AccountsRepository;
use Texyon\Database\PortalBundle\Entity\Repository\StatusRepository;
use AppBundle\Event\Collections\ProfileEvents;
use AppBundle\Event\ProfileEvent;
use Texyon\Database\PortalBundle\Entity\Accounts;

/**
 * Description of LoginHandler
 *
 */
class ValidateAccountHandler
{
    /** @var \GuzzleHttp\Client  */
    private $clientTokens;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;
    /** @var AccountsRepository */
    private $accountRepository;    
    /** @var StatusRepository */
    private $statusRepository;    
    
    
    public function __construct(
        Client $clientTokens,
        EventDispatcherInterface $eventDispatcher,
        AccountsRepository $accountRepository,
        StatusRepository $statusRepository
    ) {
        $this->clientTokens = $clientTokens;
        $this->eventDispatcher = $eventDispatcher;
        $this->accountRepository = $accountRepository;
        $this->statusRepository = $statusRepository;
    }
    
    /**
     * 
     * @param string $token
     * @param string $clientIp
     * @return Accounts
     * @throws BadRequestHttpException
     */
    public function handle($token, $clientIp)
    {
        $data = $this->getTokenData($token, $clientIp);
        $account = $this->accountRepository->findOneByEmail($data['email']);  

        if(!$account){
            throw new BadRequestHttpException('Account not found.');
        }

        $this->validateAccount($account, $clientIp);
        
        return $account;
    }
    
    private function getTokenData($token, $clientIp) 
    {
        $response = $this->clientTokens->get($token, ['query'=> [                
            'referer' => $clientIp,
        ]]);
        $data = json_decode($response->getBody(), true);
        
        return $data;
    }
    
    private function validateAccount(Accounts $account, $clientIp)
    {        
        $status = $this->statusRepository->findActiveStatus();        
        $account->setValidateUserOneStep($status, $clientIp);
        $this->accountRepository->save($account);

        $this->eventDispatcher->dispatch(
            ProfileEvents::PROFILE_SET_ACCOUNT_STATUS,
            new ProfileEvent($account)
        );
    }
}
