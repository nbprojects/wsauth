<?php

namespace AppBundle\Controller\Handler;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Description of RefreshTokenHandler
 *
 */
class RefreshTokenHandler
{
    /** @var \GuzzleHttp\Client  */
    private $clientOauth;
        
    
    public function __construct(
        Client $clientOauth
    ) {
        $this->clientOauth = $clientOauth;
    }
    
    /**
     * 
     * @param string $refresh_token
     */
    public function handle($refresh_token)
    {
        try{
            $config = $this->clientOauth->getConfig();
            
            $options = [
                'query' => array_merge(
                    $config['query'],
                    [    
                        'grant_type' => 'refresh_token',
                        'refresh_token' => $refresh_token,
                    ])
            ];

            $response = $this->clientOauth->get('token', $options);            

            return json_decode($response->getBody(), true);
        }catch (ClientException $ex) {
            $client_ex = $ex->getResponseBodySummary( $ex->getResponse() );            
            throw new BadRequestHttpException( $client_ex, $ex);
        }
    }
}
