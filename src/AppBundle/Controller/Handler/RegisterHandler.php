<?php

namespace AppBundle\Controller\Handler;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use AppBundle\Services\Security\RegisterUser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

/**
 * Description of RegisterHandler
 *
 */
class RegisterHandler
{
    /** @var \GuzzleHttp\Client */
    private $clientOauth;
    /** @var RegisterUser */
    private $registerUser;
    /** @var \GuzzleHttp\Client */
    private $wsEmailClient;
    /** @var \GuzzleHttp\Client */
    private $wsTokensClient;
    /** @var Router */
    private $router;
        
    
    /**
     * 
     * @param Client        $clientOauth
     * @param RegisterUser  $registerUser
     * @param Client        $wsEmailClient
     * @param Client        $wsTokensClient
     * @param Router        $router
     */
    public function __construct(
        Client $clientOauth,
        RegisterUser $registerUser,
        Client $wsEmailClient,
        Client $wsTokensClient,
        Router $router
    ) {
        $this->clientOauth = $clientOauth;
        $this->registerUser = $registerUser;
        $this->wsEmailClient = $wsEmailClient;
        $this->wsTokensClient = $wsTokensClient;
        $this->router = $router;
    }

    /**
     * 
     * @param string  $email
     * @param string  $password
     * @param string  $uniqueNick
     * @param string  $birthDate
     * @param boolean $rtsConsent
     * @param Request $request
     * @param string  $locale
     * @return type
     * @throws BadRequestHttpException
     */
    public function handle($email, $password, $uniqueNick, $birthDate, $rtsConsent, Request $request, $locale)
    {
        try {
            $date = new \DateTime(date('Y-m-d', strtotime($birthDate)));
            $clientIp = $request->getClientIp();
            $cookies = $request->cookies;
            $httpReferer = $request->server->get('HTTP_REFERER');
            $userAgent = $request->server->get('HTTP_USER_AGENT');
            //$locale = $request->getLocale();            
            
            $account = $this->registerUser->registerOneStep($uniqueNick, $email, $password, $date, $clientIp, $cookies, $httpReferer, $userAgent, $locale, $rtsConsent);
            $UEIgamer = $this->registerUser->getUEIgamer();
            
            $this->sendValidationEmail($email, $UEIgamer, $locale);
            // Set cookies referer            
            // Enviar evento a analytics
            // Enviar mensaje a Rabbit
    
            return $account;
            
        } catch (ClientException $ex) {
            $client_ex = $ex->getResponseBodySummary($ex->getResponse());
            throw new BadRequestHttpException($client_ex, $ex);
        }
    }
    
    private function setCookiesReferer()
    {
        
    }
    
    private function sendValidationEmail($email, $UEIgamer, $locale)
    {   
        $unlockCode = $this->getToken($email, $UEIgamer);
        $urlValidation = $this->router->generate('get_validate_account', ['token'=>$unlockCode, 'lc'=> $locale, 'version'=>'v1'], Router::ABSOLUTE_URL);

        return $this->wsEmailClient->post('send', ['form_params' => [
                'type' => 'validate_account',
                'email' => $email,
                'locale' => $locale,
                'data' => ['-token-' => $urlValidation]
            ]            
        ]); 
    }
    
    private function getToken($email, $UEIgamer) {
        $extraData = [
          'email' => $email,
          'UEIgamer' => $UEIgamer
        ];

        $response = $this->wsTokensClient->post('', ['query'=> [
            'action' => 0,
            'ueiGamer' => $UEIgamer,
            'data'=> json_encode($extraData)
        ]]);
        
        $data = json_decode($response->getBody(), true);

        return $data['code'];
    }
    
    private function sendEventAnalytics()
    {
        
    }
    
    
    
    private function login($username, $password)
    {
        $config = $this->clientOauth->getConfig();

        $options = [
            'query' => array_merge(
                $config['query'],
                [
                    'username' => $username,
                    'password' => $password,
                    'grant_type' => 'password',
                    'scope' => 'user',
                ])
        ];

        $response = $this->clientOauth->get('token', $options);

        return json_decode($response->getBody(), true);
    }
}
