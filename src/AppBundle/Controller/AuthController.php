<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\Post;
use AppBundle\Services\Collections\MemcacheCollection;
use Symfony\Component\ExpressionLanguage\Expression;


class AuthController extends FOSRestController
{
    /**
     * Register
     *
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Register in Texyon Games",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Returned when fails"
     *   }
     * )
     * 
     * @RequestParam(name="email", nullable=false, strict=false, description="User email.")
     * @RequestParam(name="password", nullable=false, strict=false, description="User password.")
     * @RequestParam(name="uniqueNick", nullable=false, strict=false, description="User nickname.")
     * @RequestParam(name="birthDate", nullable=false, strict=false, description="User birthdate. Format: yyyy-MM-dd")
     * @RequestParam(name="rtsConsent", nullable=false, strict=false, description="Email consent.")
     * @RequestParam(name="g-recaptcha-response", nullable=true, strict=false, description="Recaptcha code")
     * @RequestParam(name="locale", nullable=true, default="en", description="Language.")
     *
     * @Post("/register") 
     * 
     */
    public function postRegisterAction(ParamFetcher $paramFetcher, Request $request)
    {
        $email = $paramFetcher->get('email');
        $password = $paramFetcher->get('password');
        $uniqueNick = $paramFetcher->get('uniqueNick');
        $birthDate = $paramFetcher->get('birthDate');
        $rtsConsent = $paramFetcher->get('rtsConsent');
        //$terms = $paramFetcher->get('terms');
        $recaptcha = $paramFetcher->get('g-recaptcha-response');
        $locale = $paramFetcher->get('locale');
 
   /*     $client = $this->get('csa_guzzle.client.recaptcha');
        $res = $client->request('POST', "", ['form_params' => [
            'secret' => '6LfjiV8UAAAAANiXA7ObU0UsHn8Xx7YWIZKUDqMa',
            'response' => $recaptcha
            //'remoteip' => $ipAddress
        ]]);
        
        return json_decode($res->getBody(), true);
   */     
        $data = [
            'uniqueNick' => $uniqueNick,
            'email' => $email,
            'password' => $password,
            //'terms' => !!$terms,
            'profile' => [
                'birthDate' => $birthDate,
                'rtsConsent' => $rtsConsent,
            ],
            'recaptcha' => $recaptcha
        ];

        $registerOnestepNickType = '\AppBundle\Form\RegisterOneStepUniqueNickType';
        $registerOneStepNick = new \AppBundle\Form\Model\RegisterOneStepUniqueNick();
        $form = $this->createForm($registerOnestepNickType, $registerOneStepNick, array('csrf_protection' => false));     
        $form->submit($data);
       
        if (!$form->isValid()) {
            return View::create(array(
                'errors' => $this->buildErrorArray($form)
            ), 400);
        }

        /* @var $handler \AppBundle\Controller\Handler\RegisterHandler */
        $handler = $this->get('register.handler.controller');
        $user = $handler->handle($email, $password, $uniqueNick, $birthDate, $rtsConsent, $request, $locale);
        
        return "The user has been created successfully";
    }


    private function buildErrorArray(\Symfony\Component\Form\FormInterface $form)
    {
        $errors = [];

        foreach ($form->all() as $child) {
            $errors = array_merge(
                $errors,
                $this->buildErrorArray($child)
            );
        }
        
        $translator = $this->get('translator');
        foreach ($form->getErrors() as $error) {
            $propertyPath = str_replace(['children[profile].children[birthDate]'], 'birthDate', $error->getCause()->getPropertyPath());
            $path = str_replace(['data.', 'profile.'], '', $propertyPath);
            $errors[$path] = $translator->trans($error->getMessage());
        }

        return $errors;
    }

    
    /**
     * Login
     *
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Oauth2 login",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     * 
     * @QueryParam(name="username", nullable=false, strict=true, description="Unique user identifier.")
     * @QueryParam(name="password", nullable=false, strict=true, description="Unique user identifier.")
     *
     * 
     */
    public function getLoginAction(ParamFetcher $paramFetcher, Request $request)
    {
        $username = $paramFetcher->get('username');
        $password = $paramFetcher->get('password');
        
        /* @var $handler \AppBundle\Controller\Handler\LoginHandler */
        $handler = $this->get('login.handler.controller');
        $data = $handler->handle($username, $password);
        
        return $data;
    }
    
    /**
     * Check uniquenick
     *
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Check uniqueNick",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     * 
     * @QueryParam(name="nick", nullable=false, strict=true, allowBlank=false, description="User nickname.")
     *
     * 
     */
    public function getCheckUniquenickAction(ParamFetcher $paramFetcher)
    {
        $uniqueNick_md5 = $paramFetcher->get('nick');

        $exist = $this->get('memcache.default')->get(MemcacheCollection::PREFIX_UNIQUE_NICK . $uniqueNick_md5);
        $trans = ($exist)? 'NICK_EXISTS' : 'NICK_FREE';
        $message = $this->get('translator')->trans($trans, []);

        return array(
            'message'=>$message, 
            'hit'=>$exist
        );       
    }
    
    /**
     * Check email
     *
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "check email",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     * 
     * @QueryParam(name="email", nullable=false, strict=true, allowBlank=false, description="Email.")
     *
     * 
     */
    public function getCheckEmailAction(ParamFetcher $paramFetcher)
    {
        $email_md5 = $paramFetcher->get('email');
      
        $exist = $this->get('memcache.default')->get(MemcacheCollection::PREFIX_EMAIL . $email_md5);
        $trans = ($exist)? 'EMAIL_EXISTS' : 'EMAIL_FREE';
        $message = $this->get('translator')->trans($trans, []);

        return array(
            'message'=>$message, 
            'hit'=>$exist
        );      
    }
    
    /**
     * Refresh token
     * 
     *
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Refresh token Oauth2",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     * 
     * @QueryParam(name="refresh_token", nullable=false, strict=true, description="Unique user identifier.")
     *
     * 
     */
    public function getRefreshtokenAction(ParamFetcher $paramFetcher)
    {
        $refresh_token = $paramFetcher->get('refresh_token');
        
        /* @var $handler \AppBundle\Controller\Handler\RefreshTokenHandler */
        $handler = $this->get('refresh.token.handler.controller');
        $data = $handler->handle($refresh_token);
        
        return $data;
    }
    
    /**
     * Validate account
     * 
     *
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Validate account",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     * 
     * @QueryParam(name="token", nullable=false, strict=true, description="Code to validate")
     * ---QueryParam(name="email", nullable=false, strict=false, description="User email")
     * @QueryParam(name="lc", nullable=false, strict=true, description="User language")
     *
     * 
     */
    public function getValidateAccountAction(ParamFetcher $paramFetcher, Request $request)
    {
        try{
            $token = $paramFetcher->get('token');
            //$email = $paramFetcher->get('email');
            $lc = $paramFetcher->get('lc');
            
            /* @var $handler \AppBundle\Controller\Handler\ValidateAccountHandler */
           $handler = $this->get('validate.account.handler.controller');
           $account = $handler->handle($token, $request->getClientIp());
           
            //TODO: FALTA NOTIFICACION ENVIO DE EVENTO A HUBSPOT Y GOOGLE ANALYCS
    
            //TODO: ENVIAR EMAIL DE GRACIAS POR VALIDARTE
        /*$clientEmail = $this->get("csa_guzzle.client.ws_email_send");
        $response = $clientEmail->post('send', ['form_params' => [
                        'type' => 'ValidatedAccount',
                        'email' => $email,
                        'locale' => $locale,
                        'data' => ['-token-' => $urlValidate]
                    ]            
                ]); 
            */

            return $this->redirect( $this->getParameter('validate_account_url_success') );
            
        } catch (\Exception $ex) {
            $url = $this->getParameter('validate_account_url_error') . "?code=request_error";

            return $this->redirect($url);
        }
    }
    
    /**
     * Request validate account
     *      
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Refresh token Oauth2",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     * 
     * @QueryParam(name="token", nullable=false, strict=true, description="Token")
     *
     * 
     */
    public function getValidateTokenAction(ParamFetcher $paramFetcher)
    {
        $token = $paramFetcher->get('token');        
        
        /* @var $tokenManager \AppBundle\Services\Token\TokenManager */
        $tokenManager = $this->get('token.manager');
        $tokenEntity = $tokenManager->checkValidityByHash($token);
        
        return [
            'token' => $token,
            'isValid' => !is_null($tokenEntity)
        ];
    }
    
    /**
     * Request validate account
     *      
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Solicitud de validación de cuenta",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     * 
     * @QueryParam(name="email", nullable=false, strict=true, description="Email")
     * @QueryParam(name="locale", nullable=false, strict=true, description="User language")
     * @QueryParam(name="ipaddress", nullable=false, strict=true, description="Client ip address")
     * 
     */
    public function getRequestValidateAccountAction(ParamFetcher $paramFetcher)
    {        
        $email = $paramFetcher->get('email');
        $locale = $paramFetcher->get('locale');
        $ipAddress = $paramFetcher->get('ipaddress');

        $account = $this->get('tx.portal.accounts.repository')->findOneByEmail($email);      
        
        if(!$account){
            throw new BadRequestHttpException(sprintf("Email '%s' does not exist", $email));
        }

//TODO: COMPROBAR SI TIENE UN TOKEN VALIDO. SI TIENE YA UNO SE LE REENVIA, SINO SE LE CREA UNO NUEVO Y SE LE ENVIA
        /* @var $tokenManager \PrivateBundle\Services\Token\TokenManager */
        //$tokenManager = $this->get('token.manager');
        //$token = $tokenManager->createLongTimeRegisterOneStepToken($account, $ipAddress);
        
        /* @var $ueiManager \Texyon\Managers\Lib\UeiManager */
        $ueiManager = $this->get('uei_manager');
        $UEIgamer = $ueiManager->getTokenUei($account->getId());
        
        $response = $this->get('csa_guzzle.client.ws_tokens_private')->post('', ['query'=> [
            'action' => 0,
            'ueiGamer' => $UEIgamer,
            'data'=> json_encode(['email' => $email, 'UEIgamer' => $UEIgamer])
        ]]);
        
        $data = json_decode($response->getBody(), true);

        $token =  $data['code']; // $token->getToken()
                
        $urlValidate = $this->generateUrl('get_validate_account', [
            'token' => $token,
            'lc' => $locale,
            'email' => $email,
            'version' => 'v1'
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        return [
            'result' => 'OK',
            'message' => 'Validation email sent',
            'info' => 'TODO: hacer llamada a wsEmail de validateAccount, cuando esté creado'
        ];
                
       //TODO: hacer llamada a wsemail de validateAccount, cuando esté creado 

        /*$clientEmail = $this->get("csa_guzzle.client.ws_email_send");
        $response = $clientEmail->post('send', ['form_params' => [
                        'type' => 'ValidateAccount',
                        'email' => $email,
                        'locale' => $locale,
                        'data' => ['-token-' => $urlValidate]
                    ]            
                ]); 
        
        return [
            'result' => true
        ];*/
    }
    
    /**
     * Request password forgot
     * 
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Solicitud recuperaciñon de contraseña",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     *      
     * @QueryParam(name="email", nullable=false, strict=true, description="Email")
     * @QueryParam(name="locale", nullable=true, default="en", description="User language")
     * @QueryParam(name="destination", nullable=false, strict=true, description="Destination Url")
     * 
     */
    public function getRequestPasswordForgotAction(ParamFetcher $paramFetcher, Request $request)
    {
        $email = $paramFetcher->get('email');
        $locale = $paramFetcher->get('locale');
        $destination = $paramFetcher->get('destination');
        $ipAddress = $request->getClientIp();    
        
        $account = $this->get('tx.portal.accounts.repository')->findOneByEmail($email);      
        
        if(!$account){
            throw new BadRequestHttpException(sprintf("Email '%s' does not exist", $email));
        }
        
        /* @var $handler \AppBundle\Controller\Handler\RequestPasswordResetHandler */
        $handler = $this->get("request.password.reset.handler.controller");
        $handler->handle($account, $locale, $destination, $ipAddress);
        
        return [
            'result' => 'OK',
            'message' => 'ForgotPassword email sent'            
        ];        
    }
    
    /**
     * Request password reset
     * 
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Solicitud cambio contraseña",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     *     
     * 
     * @QueryParam(name="locale", nullable=true, default="en", description="User language")
     * @QueryParam(name="destination", nullable=false, strict=true, description="Destination Url")
     * 
     */
    public function getRequestPasswordResetAction(ParamFetcher $paramFetcher, Request $request)
    {        
        $locale = $paramFetcher->get('locale');
        $destination = $paramFetcher->get('destination');
        $ipAddress = $request->getClientIp();    
        
        /* @var $token PostAuthenticationGuardToken */
        $token = $this->get('security.token_storage')->getToken();
        $UEIgamer = $token->getUser()->getUEIgamer();

        /* @var $ueiManager \Texyon\Managers\Lib\UeiManager */
        $ueiManager = $this->get('uei_manager');
        $accountId = $ueiManager->getIdGamerByUEI($UEIgamer);
        $account = $this->get('tx.portal.accounts.repository')->find($accountId);

        /* @var $handler \AppBundle\Controller\Handler\RequestPasswordResetHandler */
        $handler = $this->get("request.password.reset.handler.controller");
        $handler->handle($account, $locale, $destination, $ipAddress);
        
        return [
            'result' => 'OK',            
            'message' => 'ResetPassword email sent',            
        ];        
    }
    
    /**
     * Request changebirthdate
     * 
     * @ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Solicitud cambio fecha de nacimiento",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     *     
     * 
     * @QueryParam(name="locale", nullable=true, default="en", description="User language")
     * @QueryParam(name="destination", nullable=false, strict=true, description="Destination Url")
     * 
     */
    public function getRequestChangeBirthdateAction(ParamFetcher $paramFetcher, Request $request)
    {        
        $locale = $paramFetcher->get('locale');
        $destination = $paramFetcher->get('destination');
        $ipAddress = $request->getClientIp();    
        
        /* @var $token PostAuthenticationGuardToken */
        $token = $this->get('security.token_storage')->getToken();
        $UEIgamer = $token->getUser()->getUEIgamer();

        /* @var $ueiManager \Texyon\Managers\Lib\UeiManager */
        $ueiManager = $this->get('uei_manager');
        $accountId = $ueiManager->getIdGamerByUEI($UEIgamer);
        $account = $this->get('tx.portal.accounts.repository')->find($accountId);

        /* @var $handler \AppBundle\Controller\Handler\RequestChangeBirthdayHandler */
        $handler = $this->get("request.change.birthday.handler.controller");
        $handler->handle($account, $locale, $destination, $ipAddress);
        
        return [
            'result' => 'OK',            
            'message' => 'Change birthday email sent',            
        ];        
    }
   
    /**
     * Login token
     * 
     *@ApiDoc(
     *   resource = true,
     *   views = { "default"},
     *   description = "Login token",
     *   statusCodes = {
     *     200 = {"Returned when successful"},
     *     403 = {"Returned when fails"}
     *   }
     * )
     * 
     * @QueryParam(name="tx_pk", nullable=false, strict=true, description="Texyon public key")
     * @QueryParam(name="token", nullable=false, strict=true, description="Token")
     * 
     */
    /*public function getLogintokenAction(ParamFetcher $paramFetcher)
    {
        try {
            $token = $paramFetcher->get('token');
            $publicKey = $paramFetcher->get('tx_pk');

            /* @var $cryptManager \Texyon\Managers\Lib\CryptManager /
            $cryptManager = $this->get('crypt.manager');
            $dtoken = $cryptManager->decodeToken($token, $publicKey);

            /* @var $tokenChecker \AppBundle\Security\OAuthTokenChecker /
            $oauthTokenChecker = $this->get('app.oauth_token_checker');
            $oauthTokenChecker->verify($dtoken->access_token);
            
            $url = "{$this->getParameter('logintoken_uri')}/{$this->getParameter('txn_public_key')}/{$token}";

            return $this->redirect($url);      
        } catch (AuthenticationException $ex) {

            return $this->redirect('http://www.texyon.com/en/error?error=invalid_token');
        }     
    }*/
}
