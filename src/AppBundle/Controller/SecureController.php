<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Texyon\Managers\Lib\UeiManager;

class SecureController extends FOSRestController
{
    
    /**
     * Login token
     *      
     * @QueryParam(name="redirect_uri", nullable=false, strict=true, description="Token")
     * 
     */
    public function getLogintokenGenerateAction(ParamFetcher $paramFetcher)
    {        
        $redirectUri = $paramFetcher->get('redirect_uri');
        
        /* @var $token PostAuthenticationGuardToken */
        $token = $this->get('security.token_storage')->getToken();
        $user = $token->getUser();
        $UEIgamer = $user->getUEIgamer();
        $time = (3600 * 2); // 2h 
        
        $data = [
            'uei' => $UEIgamer,
            'access_token' => $user->getApiToken(),
            'url' => $redirectUri,
            'date' => date("Y-m-d H:i:s"),
            'time' => $time
        ];
 
        /* @var $cryptManager \Texyon\Managers\Lib\CryptManager */
        $cryptManager = $this->get('crypt.manager');
        $new_token = $cryptManager->encodeArrayToken($data);

        $url = $this->generateUrl('public_get_logintoken', [
            'token' => $new_token,
            'tx_pk' => $this->getParameter('txn_public_key')
        ],UrlGeneratorInterface::ABSOLUTE_URL);
        
        return [
            'url' => $url
        ];
    }
    
    /**
     * Validate account
     * 
     * @QueryParam(name="password", nullable=false, strict=true, description="Code to validate")
     * @QueryParam(name="birthday", nullable=false, strict=true, description="User email")
     * @QueryParam(name="token", nullable=true, strict=true, description="token")
     * @QueryParam(name="lc", nullable=true, strict=true, description="User language")
     * 
     */
    public function getPasswordResetAction(ParamFetcher $paramFetcher, Request $request)
    {
        $token = $paramFetcher->get('token');
        $password = $paramFetcher->get('password');
        $birthday = $paramFetcher->get('birthday');
        $lc = $paramFetcher->get('lc');
        
        /* @var $tokenStorage PostAuthenticationGuardToken */
        $tokenStorage = $this->get('security.token_storage')->getToken();
        $user = $tokenStorage->getUser();
        $UEIgamer = $user->getUEIgamer();
              
            
            /* @var $ueiManager UeiManager */
        $ueiManager = $this->get('uei_manager');
        $accountId = $ueiManager->getIdGamerByUEI($UEIgamer);
        //$accountId = 5;
return 'passa';
        $accountRepo = $this->get('tx.portal.accounts.repository');
        /* @var $account \Texyon\Database\PortalBundle\Entity\Accounts */
        $account = $accountRepo->find($accountId);     

        /* @var $tokenManager \PrivateBundle\Services\Token\TokenManager */
        $tokenManager = $this->get('token.manager');            
        $tokenEntity = $tokenManager->checkValidityByHash($token);

        if(!$tokenEntity){
            throw new BadRequestHttpException('Token not valid.');
        }

        if( $accountId !== $tokenEntity->getAccountId()->getId() ){
            throw new BadRequestHttpException('Token not valid for the account.');
        }

        $account->setEncoder( $this->get('security.encoder_factory') );
        $newPassword = $account->changePassword($password);

        $em = $this->getDoctrine()->getManager();
        $em->persist($newPassword);
        $em->flush();

        $tokenManager->consumeToken($token);

        return [
            'result' => 'OK',
            'message' => $this->get('translator')->trans('CHANGE_PASSWORD_SUCCESFULLY')
        ];

    }
}
