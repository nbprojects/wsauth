<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/js")
 */
class JavascriptController extends Controller
{

    /**
     * @Route("/api.js")
     */
    public function authAction()
    {
        $params = array( 'test' => 'ok' );
        $rendered = $this->renderView( 'AppBundle:Javascript:api.js.twig', $params );
        $response = new Response( $rendered );
        $response->headers->set( 'Content-Type', 'text/javascript' );

        return $response;
    }

}
