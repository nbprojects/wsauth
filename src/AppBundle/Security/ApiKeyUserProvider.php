<?php

namespace AppBundle\Security;


use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use AppBundle\Security\User;


class ApiKeyUserProvider implements UserProviderInterface
{
    protected $apiKey;
    protected $UEIgamer;
    /** @var OAuthTokenChecker */
    private $oauthTokenChecker;    

    /**
     * 
     * @param OAuthTokenChecker $oauthTokenChecker
     */
    public function __construct(
        OAuthTokenChecker $oauthTokenChecker
    )
    {
        $this->oauthTokenChecker = $oauthTokenChecker;
    }
    
    public function getUsernameForApiKey($apiKey)
    {
        if (!$apiKey) {
            return 'anonymous';
        }
   
        $this->apiKey = $apiKey;
        $token =  $this->oauthTokenChecker->verify($apiKey);

        if( !isset($token['username'])){
            return;
        }
      
        if( isset($token['username'])){
            $this->UEIgamer = $token['UEIgamer'];
            //$this->accountId = $token['account_id'];
            //$this->accountId = $this->getAccountByUEIgamer( $this->UEIgamer );            
        }
        
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
        
        return $token['username']; 
    }
               
    
    public function loadUserByUsername($username)
    {
        /*return new User(
            $username,
            null,
            // the roles for the user - you may choose to determine
            // these dynamically somehow based on the user
            array('ROLE_USER')
        );*/
  
        return new User(
            $username,
            null, // pass
            null, // salt
            // the roles for the user - you may choose to determine
            // these dynamically somehow based on the user
            array('ROLE_OAUTH2'),
            $this->apiKey,
            $this->UEIgamer,
            null //$this->accountId
        );
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
    
}