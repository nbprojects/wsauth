<?php

namespace AppBundle\Security;

use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\Validator\RecursiveValidator as ConstraintValidator;
use portal\PublicBundle\Services\Security\RegisterUser;
use Tools\CookiesBundle\Services\RefererManager;
use Core\CoreDomain\User\User\Accounts;
use portal\PublicBundle\DomainManager\AccountsManager;
use portal\PublicBundle\DomainManager\ProfileManager;
use Texyon\Managers\Lib\UeiManager;

/**
 * Class ValidatorAccount
 * @package portal\PublicBundle\Controller\Handler
 */
class ValidatorAccount 
{
    /** @var ConstraintValidator */
    private $constraintValidator;
    /** @var RegisterUser */
    private $registerUser;
    /** @var RefererManager */
    private $refererManager;
    /** @var AccountsManager  */
    private $accountsManager;
    /** @var ProfileManager  */
    private $profileManager;
    /** @var UeiManager */
    private $ueiManager;
    
    
    /**
     * 
     * @param ConstraintValidator   $constraintValidator
     * @param RegisterUser          $register
     * @param RefererManager        $refererManager
     * @param AccountsManager       $accountsManager
     * @param ProfileManager        $profileManager
     * @param UeiManager            $ueiManager
     */
    public function __construct(
        ConstraintValidator $constraintValidator,
        RegisterUser $register,
        RefererManager $refererManager,
        AccountsManager $accountsManager,
        ProfileManager $profileManager,
        UeiManager $ueiManager
    ){
        $this->constraintValidator = $constraintValidator;
        $this->registerUser = $register;
        $this->refererManager = $refererManager;
        $this->accountsManager = $accountsManager;
        $this->profileManager = $profileManager;
        $this->ueiManager = $ueiManager;
    }
    
    /**
     * 
     * @param string $referers_json
     * @throws \Exception
     */
    private function checkReferers(&$referers)
    {
        $referers = json_decode($referers, true);

        if ($referers === null) {
            throw new \Exception("Invalid json format for referers.");
        }

    }
    
    /**
     * 
     * @param string $email
     *
     * @throws \Exception
     */
    private function validateEmail($email)
    {
        $emailConstraint = new EmailConstraint();
        $emailConstraint->message = 'Invalid email address';

        $errors = $this->constraintValidator->validate($email, $emailConstraint);
        
        if ($errors->count() > 0) {
            throw new \Exception($errors->get(0)->getMessage());
        }
        
        $user = $this->accountsManager->getRepository()->findOneBy(array(
            'email'=> $email
        ));
        
        if ($user) {
            throw new \Exception('Email already exists');
        }
        
    }
    
   
    /**
     * 
     * @param string $nick
     * @throws \Exception
     */
    private function validateUniqueNick($nick)
    {
        $uniqueNick = $this->profileManager->getRepository()->findOneBy(array(
            'uniqueNick' => $nick
        ));
        
        if ($uniqueNick) {
            throw new \Exception('UniqueNick already exists');
        }
            
    }

}
