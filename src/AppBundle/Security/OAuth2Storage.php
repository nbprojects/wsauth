<?php

namespace AppBundle\Security;

use OAuth2\IOAuth2Storage;
use OAuth2\Model\IOAuth2Client;


/**
 * Description of OAuth2Storage
 *
 * @author Ignacio
 */
class OAuth2Storage implements IOAuth2Storage
{
    //put your code here
    
    public function getClient($clientId){
        
    }
    
   public function checkClientCredentials(IOAuth2Client $client, $clientSecret = null){}
    
    public function getAccessToken($oauthToken){}


    public function createAccessToken($oauthToken, IOAuth2Client $client, $data, $expires, $scope = null){}


    public function checkRestrictedGrantType(IOAuth2Client $client, $grantType){}

      
}
