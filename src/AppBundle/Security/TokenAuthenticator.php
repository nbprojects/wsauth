<?php

namespace AppBundle\Security;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use AppBundle\Security\OAuthTokenChecker;


class TokenAuthenticator extends AbstractGuardAuthenticator
{
    /** @var OAuthTokenChecker */
    private $oauthTokenChecker;    

    /**
     * 
     * @param OAuthTokenChecker $oauthTokenChecker
     */
    public function __construct(
        OAuthTokenChecker $oauthTokenChecker
    )
    {
        $this->oauthTokenChecker = $oauthTokenChecker;
    }
    
    /**
     * Llamado en cada request. Devolverá las credenciales que quieras,
     * o null para parar la autenticación.
     */
    public function getCredentials(Request $request)
    {        
        $token = $this->oauthTokenChecker->serverService->getBearerToken($request, true);
        
       /* if( !$token ){
            return; // no hay token? Devuelve null y no se llamará a otros métodos
        }*/

        return array(
            'token' => $token,
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        //$verify = $this->verifyToken($credentials); $verify['username']
        $apiKey = $credentials['token'];
        $username = $userProvider->getUsernameForApiKey( $apiKey );
    
        if( $username == null) {
            return;
        }else if($username === 'anonymous') {
            return new User(
                $username,
                null,
                null,
                array('IS_AUTHENTICATED_ANONYMOUSLY'),
                '',
                null,
                null
            );            
        }
        
        
        return $userProvider->loadUserByUsername( $username );
        
        // si es null, la autenticación fallará
        // si es un objeto User se llama a checkCredentials()
//        return $this->em->getRepository('AppBundle:User')
//            ->findOneBy(array('apiKey' => $apiKey));
        
        /*return new User(
            $verify['username'],
            null,
            null,
            // the roles for the user - you may choose to determine
            // these dynamically somehow based on the user
            array('ROLE_USER'),
            $credentials
        );*/
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        //$verify = $this->checkToken($credentials);
        
        // comprueba las credenciales - e.g. asegurar que el password es válido
       /* if ($verify == false) {
            // returning anything NOT true will cause an authentication failure
            return;
            // or, you can still throw an AuthenticationException if you want to
            // throw new AuthenticationException();
        }*/

        // devuelve true con autenticación exitosa
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {

        // si es exitoso, dejar que el request continúe
        return;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $msg = json_decode($exception->getMessage());
        $error = $msg? : ['error'=>$exception->getMessageKey(), 'error_description'=>$exception->getMessage()];
        
        $data = array(
            'error' => 403,
            'message' => $error //strtr($exception->getMessageKey(), $exception->getMessageData())
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        );

        return new JsonResponse($data, 403);
    }

    /**
     * Llamado cuando se necesita autenticación pero no se ha enviado
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            'error' => 401,
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, 401);
    }

    public function supportsRememberMe()
    {
        return false;
    }
    
    
     
}