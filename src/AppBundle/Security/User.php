<?php
namespace AppBundle\Security;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User implements UserInterface 
{

    private $username;
    private $password;
    private $salt;
    private $roles;
    private $apiToken;
    private $UEIgamer;
    private $accountId;
    
    
    public function __construct($username, $password, $salt, array $roles, $apiToken, $UEIgamer, $accountId)
    {
        $this->username = $username;
        $this->password = $password;
        $this->salt = $salt;
        $this->roles = $roles;
        $this->apiToken = $apiToken;
        $this->UEIgamer = $UEIgamer;
        $this->accountId = $accountId;
        
    }
    
    public function getUsername()
    {
        return $this->username;
    }

    public function getRoles()
    {
        return $this->roles; //['ROLE_USER'];
    }

    public function getPassword()
    {
    }
    public function getSalt()
    {
    }
    public function eraseCredentials()
    {
    }
    
    public function getApiToken()
    {
        return $this->apiToken;
    }
    // más getters y setters
    
    public function getUEIgamer()
    {
        return $this->UEIgamer;
    }

    public function getAcountId()
    {
        return $this->accountId;
    }
}