<?php

namespace AppBundle\Security;

use OAuth2\OAuth2;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;


/**
 * Description of OAuthTokenChecker
 *
 * @author NachoBordon
 */
class OAuthTokenChecker
{
    /** @var OAuth2 */
    public $serverService; 
    /** @var Client  */
    private $clientOauth;
    
    
    /**
     * 
     * @param OAuth2 $server
     * @param Client $clientOauth
     */
    public function __construct(
        OAuth2 $server,
        Client $clientOauth
    ) {
        $this->serverService = $server;
        $this->clientOauth = $clientOauth;
    }
        
    public function verify($oauthToken)
    {        
        try{
            $params = [
                'access_token' => $oauthToken
            ];
   
            $response = $this->clientOauth->get('tokeninfo', ['query' => $params]);

            return json_decode($response->getBody(), true);
            
        }catch (ClientException $ex) {            
            $client_ex = $ex->getResponseBodySummary( $ex->getResponse() );
            throw new AuthenticationException($client_ex, $ex->getCode());   
        }
    }
}
