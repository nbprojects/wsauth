Api Auth js
===================

## Examples

```javascript

        <script src="http://api.texyon.com/auth/js/auth.js"></script>

        var auth = new Auth();                    

        auth.signIn('user1@texyon.com','123456789').then(( res )=> {
            console.log( "Data Saved: ", res );
        });

        auth.refreshToken().then((res)=>{
            console.log(res);
        });
```