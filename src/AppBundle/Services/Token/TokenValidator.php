<?php

namespace AppBundle\Services\Token;

use Texyon\Database\PortalBundle\Entity\Tokens;


/**
 * Class TokenValidator
 * @package portal\PublicBundle\Services\Token
 */
class TokenValidator
{
    const DAYS_TOKEN_EXPIRATION = 30;

    /**
     * @param Tokens $token
     *
     * @return bool
     */
    public function check(Tokens $token)
    {
        if (true === $this->isExpirated($token->getCreatedAt())) {
            return false;
        }

        if (true === $this->isConsumed($token->getConsumedAt())) {
            return false;
        }

        return true;
    }

    /**
     * @param \DateTime|null $consumedAt
     *
     * @return bool
     */
    //TODO, refactor when change token entity for memcached system.
    private function isConsumed($consumedAt)
    {
        if ($consumedAt instanceof \DateTime) {
            return true;
        }

        return false;
    }

    /**
     * @param \DateTime $dateTime
     *
     * @return bool
     */
    private function isExpirated(\DateTime $dateTime)
    {
        $interval = sprintf('P%sD', self::DAYS_TOKEN_EXPIRATION);
        $dateTime->add(new \DateInterval($interval));

        $now = new \DateTime();

        if ($now->getTimestamp() > $dateTime->getTimestamp()) {
            return true;
        }

        return false;
    }
}