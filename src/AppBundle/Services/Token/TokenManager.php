<?php

namespace AppBundle\Services\Token;

use Texyon\Database\PortalBundle\Doctrine\TokenInterface;
use Texyon\Database\PortalBundle\Entity\Repository\TokensRepository;
use Texyon\Database\PortalBundle\Entity\Accounts;
use Texyon\Database\PortalBundle\Entity\Tokens;
use AppBundle\Services\Token\TokenValidator;

/**
 * Class TokenManager
 * @package portal\PublicBundle\Services\Token
 */
class TokenManager
{
    const REGISTER_ACTION = 0;
    const CHANGE_PASSWORD_ACTION = 1;
    const CHANGE_BIRTHDATE_ACTION = 2;
    const REGISTER_ONE_STEP_ACTION = 3;
    
    /** @var TokensRepository */
    private $tokensRepository;
    /** @var \portal\PublicBundle\Services\Token\TokenValidator  */
    private $tokenValidator;
    private $daysTokenExpiration;

    /**
     * @param TokensRepository $tokensManager
     * @param TokenValidator $tokenValidator
     */
    public function __construct(
        TokensRepository $tokensManager, 
        TokenValidator $tokenValidator,
        $days_token_expiration
    )
    {
        $this->tokensRepository = $tokensManager;
        $this->tokenValidator = $tokenValidator;
        $this->daysTokenExpiration = $days_token_expiration;
    }

    /**
     * @param Accounts $accounts
     *
     * @return string
     */
    public function createRegisterToken(Accounts $accounts, $ipClient)
    {
        $token = $this->createToken($accounts, self::REGISTER_ACTION, $ipClient);

        return $token->getToken();

    }

    /**
     * @param Accounts $accounts
     * @param string   $ipClient
     *
     * @return string
     */
    public function createRegisterOneStepToken(Accounts $accounts, $ipClient)
    {
        $token = $this->createToken($accounts, self::REGISTER_ONE_STEP_ACTION, $ipClient);

        return $token->getToken();

    }

    /**
     * @param Accounts $accounts
     * @param string   $ipClient
     *
     * @return string
     */
    public function remakeLastToken(Accounts $accounts, $ipClient)
    {
        $actions = array(
            self::REGISTER_ACTION,
            self::REGISTER_ONE_STEP_ACTION
        );
        $token = $this->createLastToken($accounts, $actions, $ipClient);

        return $token->getToken();

    }

    /**
     * @param Accounts  $accounts
     * @param string    $ipClient
     *
     * @return Tokens|null
     */
    public function createLongTimeRegisterToken(Accounts $accounts, $ipClient)
    {
        return $this->createToken($accounts, self::REGISTER_ACTION, $ipClient);
    }

    /**
     * @param Accounts  $accounts
     * @param string    $ipClient
     *
     * @return Tokens|null
     */
    public function createLongTimeRegisterOneStepToken(Accounts $accounts, $ipClient)
    {
        return $this->createToken($accounts, self::REGISTER_ONE_STEP_ACTION, $ipClient);
    }

    /**
     * @param Accounts  $accounts
     * @param string    $ipClient
     *
     * @return string
     */
    public function createChangePasswordToken(Accounts $accounts, $ipClient)
    {
        $token = $this->createToken($accounts, self::CHANGE_PASSWORD_ACTION, $ipClient);

        return $token->getToken();
    }

    /**
     * @param Accounts $accounts
     * @param string   $ipClient
     *
     * @return string
     */
    public function createChangeBirthdateToken(Accounts $accounts, $ipClient)
    {
        $token = $this->createToken($accounts, self::CHANGE_BIRTHDATE_ACTION, $ipClient);

        return $token->getToken();
    }

    public function checkValidityByHash($tokenHash) 
    {
        $token = $this->getTokenByHash($tokenHash);        
        
        if ( $token !== null && $this->tokenValidator->check($token) ) {
           return $token;
        }    
    }
    
    /**
     * Check validate token. If don't exist token, create new.
     *
     * @param int $user
     * @param int $action
     *
     * @return Tokens
     */
    public function checkToken($user, $action = 0)
    {
        $checktoken = $this->getToken($user, $action);

        if( is_null($checktoken) ){
            return $this->createNewToken($action, $user);
        }else{
            //if token is invalid(consumed or expired), create new token.
            if( !$this->tokenValidator->check($token) ){
                return $this->createNewToken($action, $user);
            }
        }

        return $checktoken;
    }
          
    /**
     * Consume token.
     *
     * @param $authtoken
     */
    public function consumeToken($authtoken)
    {
        $token = $this->getTokenByHash($authtoken);
        $token->setConsumedAt(new \DateTime());
        
        $this->tokensRepository->save($token);
    }
    
    /**
     * @param Accounts $user
     * @param int      $action
     * @param string   $ipClient
     *
     * @return Tokens
     */
    private function createNewToken(Accounts $user, $action, $ipClient)
    {
        $token = new Tokens();
        $token->setNewToken($user, $action, $ipClient);

        $this->tokensRepository->save($token);

        return $token;
    }

    /**
     * @param Accounts $user
     * @param int      $action
     *
     * @return null|Tokens
     */
    private function getToken(Accounts $user, $action)
    {
        return $this->tokensRepository->findOneBy(
            array(
                'accountId' => $user,
                'action' => $action
            ),
            array('createdAt' => 'desc')
        );
    }

    /**
     * @param Accounts $user
     * @param array    $actions
     *
     * @return null|Tokens
     */
    private function getLastToken(Accounts $user, $actions)
    {
        $query = $this->tokensRepository->findLastToken($user, $actions);
        
        return $query->getOneOrNullResult();
    }

    /**
     * @param $tokenHash
     * @return Tokens
     */
    private function getTokenByHash($tokenHash)
    {
        return $this->tokensRepository->findOneBy(
            array(
                'token' => $tokenHash,
            )
        );
    } 
    
    /**
     * @param Accounts $accounts
     * @param int      $action
     * @param string   $ipClient
     *
     * @return null|Tokens
     */
    private function createToken(Accounts $accounts, $action, $ipClient)
    {
        $token = $this->getToken($accounts, $action);

        if (null === $token) {
            $token = $this->createNewToken($accounts, $action, $ipClient);

            return $token;
        }

        if (false === $this->tokenValidator->check($token)) {
            $token = $this->createNewToken($accounts, $action, $ipClient);

            return $token;
        }

        return $token;
    }
    
    
    /**
     * @param Accounts $accounts
     * @param array    $actions
     * @param string   $ipClient
     *
     * @return null|Tokens
     */
    private function createLastToken(Accounts $accounts, array $actions, $ipClient)
    {
        $token = $this->getLastToken($accounts, $actions);
                        
        if (null === $token) {
            $token = $this->createNewToken($accounts, $actions[0], $ipClient);

            return $token;
        }

        if (false === $this->tokenValidator->check($token)) {
            $token = $this->createNewToken($accounts, $token->getAction(), $ipClient);

            return $token;
        }

        return $token;
    }

    
}