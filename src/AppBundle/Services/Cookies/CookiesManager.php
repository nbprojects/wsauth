<?php

namespace AppBundle\Services\Cookies;

use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class CookiesManager
 * @package AppBundle\Services
 */
class CookiesManager
{
    const INVITE_REFERENCE = 'invite_reference';
    const TX_CHANNEL_REF = 'tx_channel_ref';
    const TX_IDENTIFICATION_LOGIN = 'tx_lgn';
    const TX_REFERER_GAME = 'tx_refererGame';

    /**
     * @param ParameterBag $cookies
     *
     * @return string|bool
     */
    public function getInviteReference(ParameterBag $cookies)
    {
        return $this->checkCookie(self::INVITE_REFERENCE, $cookies);
    }

    /**
     * @deprecated use cookieReferer instad of this method
     * @param ParameterBag $cookies
     *
     * @return bool|string
     */
    public function getChannelRef(ParameterBag $cookies)
    {
        return $this->checkCookie(self::TX_CHANNEL_REF, $cookies);
    }

    /**
     *
     * @param type $cookies
     *
     * @return bool|string
     */
    public function getLoginKey(ParameterBag $cookies)
    {
        return $this->checkCookie(self::TX_IDENTIFICATION_LOGIN, $cookies);
    }

    /**
     *
     * @param type $cookies
     *
     * @return bool|string
     */
    public function getRefererGame(ParameterBag $cookies)
    {
        return $this->checkCookie(self::TX_REFERER_GAME, $cookies);
    }

    /**
     * @param string       $cookieName
     * @param ParameterBag $cookies
     *
     * @return bool|string
     */
    private function checkCookie($cookieName, ParameterBag $cookies)
    {
        if (true === $cookies->has($cookieName)) {
            return $cookies->get($cookieName);
        }

        return false;
    }
}
