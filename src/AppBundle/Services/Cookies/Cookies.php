<?php

namespace AppBundle\Services\Cookies;

//use Symfony\Component\HttpFoundation\ParameterBag;

use Symfony\Component\HttpFoundation\ParameterBag;
use Texyon\Database\PortalBundle\Entity\Accounts;

//use Tools\CookiesBundle\Services\RefererManager;
//use Tools\CookiesBundle\Services\GameCouponManager;

/**
 * 
 */
class Cookies
{
    /** @var RefererManager */
    private $refererManager;
    /** @var GameCouponManager */
    private $gameCouponManager;
    
    
    /**
     * 
     * @param RefererManager    $refererManager
     * @param GameCouponManager $gamesCouponManager
     */
    public function __construct(
       // RefererManager $refererManager,
       // GameCouponManager $gamesCouponManager
    )
    {
        //$this->refererManager = $refererManager;
        //$this->gameCouponManager = $gamesCouponManager;
    }
    
    /**
     * 
     * @param Accounts      $account
     * @param ParameterBag  $cookies
     * @param string        $httpReferer
     * @param string        $userAgent
     */
    public function onRegister(Accounts $account, ParameterBag $cookies, $httpReferer, $userAgent)
    {
        //$this->refererManager->start($account, $cookies, $userAgent, $httpReferer);
        //$this->gameCouponManager->start($account, $cookies);
    }
    
        /**
     * 
     * @param Accounts      $account
     * @param ParameterBag  $cookies
     * @param string        $httpReferer
     * @param string        $userAgent
     */
    public function onValidate(Accounts $account, ParameterBag $cookies, $httpReferer, $userAgent)
    {
        //$this->refererManager->setIsOnValidate(true);
        //$this->refererManager->start($account, $cookies, $userAgent, $httpReferer);
    }
}
