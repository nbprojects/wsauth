<?php

namespace AppBundle\Services\Security;

use Texyon\Database\PortalBundle\Entity\Accounts;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Texyon\Database\PortalBundle\Doctrine\DoctrineDomainManager;
use Texyon\Database\PortalBundle\Entity\Status;
use Lsw\MemcacheBundle\Cache\MemcacheInterface;
use Texyon\Managers\Lib\UeiManager;
use Symfony\Component\HttpFoundation\ParameterBag;
use Texyon\Database\PortalBundle\Entity\Repository\AccountsRepository;
use Texyon\Database\PortalBundle\Entity\Repository\StatusRepository;
use AppBundle\Services\Cookies\CookiesManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 * Description of Register
 *
 * @author NachoBordon
 */
class Register
{

    /** @var EncoderFactory */
    private $encoder;
    /** @var DoctrineDomainManager */
    protected $domainManager;
    /** @var AccountsRepository */
    private $accountsRepository;
    /** @var StatusRepository */
    private $statusRepository;
    /** @var MemcacheInterface */
    private $memcache;
    /** @var UeiManager */
    private $ueiManager;
    /** @var CookiesManager */
    private $cookiesManager;
    /** @var ParameterBag */
    private $cookies;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;
    /** @var Accounts */
    public $account;
    
    
    public function __construct(
        EncoderFactory $encoderFactory,
        DoctrineDomainManager $domainManager,
        AccountsRepository $accountsRepository,
        StatusRepository $statusRepository,
        MemcacheInterface $memcache,
        UeiManager $ueiManager,
        CookiesManager $cookiesManager,
        EventDispatcherInterface $eventDispatcherInterface
    ) {
        $this->encoder = $encoderFactory;
        $this->domainManager = $domainManager;
        $this->statusRepository = $statusRepository;
        $this->accountsRepository = $accountsRepository;
        $this->memcache = $memcache;
        $this->ueiManager = $ueiManager;
        $this->cookiesManager = $cookiesManager;
        $this->eventDispatcher = $eventDispatcherInterface;
        
    }
    
    public function getAccountEntity()
    {
        $entity = new Accounts();
        $entity->setEncoder($this->encoder);

        return $entity;
    }
        
    /**
     *
     * @param string $email
     * @param string $password
     * @param \DateTime $birthDate
     * @param string $clientIp
     * @param string $locale
     * @param AccountsEntity|null $parent
     * @param string $uniqueNick
     * @param int $rtsConsent
     * @return Accounts
     */
    protected function createUserOneStep($email, $password, \DateTime $birthDate, $clientIp, $locale, $uniqueNick=null, $rtsConsent=null)
    {
        $status = $this->getInactiveStatus();
        $accounts = $this->getAccountEntity();
        $parent = $this->getParentCookie();

        $this->account = $accounts->setNewUserFromRegisterOneStep($email, $password, $birthDate, $clientIp, $status, $locale, $uniqueNick, $parent, $rtsConsent);
        
        return $this->account;
    }
    
    /**
     *
     * @return Status
     */
    private function getInactiveStatus()
    {
        //return $this->statusDomainManager->getRepository()->find(0);
        return $this->statusRepository->find(0);
    }
    
    /**
     *
     * @param string $value
     */
    protected function setValueInMemcache($value, $prefix)
    {
        if ($value !== null) {
            $key = $prefix . md5(strtolower($value));
            $this->memcache->set($key, true, 0);
        }
    }
    
    public function setCookies(ParameterBag $cookies)
    {
        $this->cookies = $cookies;
    }
    
    public function getUEIgamer()
    {
        if ($this->account) {
            return $this->ueiManager->getTokenUei($this->account->getId());
        }
    }
    
    /**
     * @return mixed
     */
    protected function getParentCookie()
    {
        $parentCookie = $this->cookiesManager->getInviteReference($this->cookies);
        
        if ($parentCookie) {
            return $this->getParentFromUei($parentCookie);
        }
    }
        
    /**
     * @param string $UEIgamer
     *
     * @return null|AccountsEntity
     */
    protected function getParentFromUei($UEIgamer)
    {
        $id = $this->ueiManager->getAccountFromUEI($UEIgamer);
        //return $this->accountsDomainManager->getRepository()->find($id);
        return $this->accountsRepository->find($id);
    }
    
    /**
     * 
     * @param ParameterBag $cookies
     * @return string
     */
    protected function getRefererGame()
    {
        //$game = ($cookies !== null)? $cookies->get('tx_refererGame') : null;
        return $this->cookiesManager->getRefererGame($this->cookies);        
    }
}
