<?php

namespace AppBundle\Services\Security;


use Texyon\Database\PortalBundle\Entity\Accounts as AccountsEntity;
use ValidateAcc

use Core\CoreDomain\User\User\Accounts as AccountsEntity;
use Core\CoreDomain\User\Model\ValidateAccounts;
use portal\PublicBundle\DomainManager\AccountsManager;
use portal\PublicBundle\Services\Token\TokenManager;
use portal\PublicBundle\Services\Security\Login;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use portal\PublicBundle\Event\HubspotEvent;
use portal\PublicBundle\Event\HubspotEvents;


/**
 * Class Validate
 * @package portal\PublicBundle\Services\Security
 */
abstract class Validate
{
       
    /** @var ValidateAccounts  */
    protected $validateAccounts;
    /** @var EventDispatcherInterface  */
    protected $eventDispatcher;
    /** @var AccountsManager  */
    protected $accountsRepository;
    /** @var  TokenManager */
    protected $tokenManager;



    public function __construct
    (
        ValidateAccounts $validateAccounts,
        AccountsManager $accountsManager,
        EventDispatcherInterface $eventDispatcherInterface,
        TokenManager $tokenManager
    )
    {
        $this->validateAccounts = $validateAccounts;
        $this->accountsDomainManager = $accountsManager;
        $this->eventDispatcher = $eventDispatcherInterface;
        $this->tokenManager = $tokenManager;
    }



   /**
     * 
     * @param string          $status
     * @param string          $ipAddressValidation
     * @param AccountsEntity  $account
     * @return type
     */
    protected function validateUser($status, $ipAddressValidation, AccountsEntity $account)
    {
        $password = $account->getPassword();
        $birthDate = $account->getProfile()->getBirthDate();
        $accounts = $this->validateAccounts->setEntityEncoder($account);
        
        $this->callHubspotEvent($account, $ipAddressValidation);
        
        return $accounts->setValidateUser($status, $birthDate, $ipAddressValidation, $password);
    }

    /**
     * 
     * @param string          $status
     * @param string          $ipAddressValidation
     * @param AccountsEntity  $account
     * @return type
     */
    protected function validateUserOneStep($status, $ipAddressValidation, AccountsEntity $account)
    {
        $accounts = $this->validateAccounts->setEntityEncoder($account);
        
        $this->callHubspotEvent($account, $ipAddressValidation);
               
        return $accounts->setValidateUserOneStep($status, $ipAddressValidation);
    }
      
    
    /**
     * 
     * @param AccountsEntity      $account
     * @param string        $clientIp
     */
    private function callHubspotEvent(AccountsEntity $account, $clientIp)
    {
        $this->eventDispatcher->dispatch(
            HubspotEvents::HUBSPOT_VALIDATE_USER,
            new HubspotEvent($account, $clientIp)
        );
    }
}
