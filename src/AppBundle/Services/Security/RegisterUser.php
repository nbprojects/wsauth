<?php

namespace AppBundle\Services\Security;

use Symfony\Component\HttpFoundation\ParameterBag;
use AppBundle\Services\Collections\MemcacheCollection;

use Texyon\Database\PortalBundle\Entity\Accounts;
use AppBundle\Event\Collections\EmailEvents;
use AppBundle\Event\EmailRegisterEvent;
use AppBundle\Event\QueueOnRegisterEvent;
use AppBundle\Event\Collections\QueueEvents;
use AppBundle\Event\Collections\ProfileEvents;
use AppBundle\Event\ProfileEvent;
use AppBundle\Event\Collections\CookiesEvents;
use AppBundle\Event\ProcessCookiesEvent;

/**
 * Description of RegisterUser
 *
 * @author NachoBordon
 */
class RegisterUser extends Register
{

    /**
     * @param string        $uniqueNick
     * @param string        $email
     * @param string        $password
     * @param \DateTime     $birthDate
     * @param string        $clientIp
     * @param ParameterBag  $cookies
     * @param string        $httpReferer
     * @param string        $userAgent
     * @param string        $referer
     * @param string        $locale
     * @param string        $uri
     *
     * @return Accounts
     */
    public function registerOneStep($uniqueNick, $email, $password, $birthDate, $clientIp, ParameterBag $cookies, $httpReferer, $userAgent, $locale, $rtsConsent=null)    
    {
    //TODO: set AccountStatus
        $this->setCookies($cookies);
        $accountEntity = $this->createUserOneStep($email, $password, $birthDate, $clientIp, $locale, $uniqueNick, $rtsConsent);
        $this->domainManager->save($accountEntity);

        //$this->createSession($accountEntity, $clientIp, $cookies, $httpReferer);
        $this->updateMemcacheValues($uniqueNick, $email);        
        
        $game = $this->getRefererGame($cookies);
        // Events
        $this->callAccountStatusEvent($accountEntity);
        $this->callEmailOneStepEvent($accountEntity, $userAgent, $clientIp, $httpReferer, $locale, $game);
        $this->callCookiesEvent($accountEntity, $cookies, $httpReferer, $userAgent);
        $this->callOnRegisterEvent($accountEntity);
        $this->callGeoIpEvent($accountEntity, $clientIp);        
        
        return $accountEntity;
    }    
    
    private function updateMemcacheValues($uniqueNick, $email)
    {
        $this->setValueInMemcache($uniqueNick, MemcacheCollection::PREFIX_UNIQUE_NICK);
        $this->setValueInMemcache($email, MemcacheCollection::PREFIX_EMAIL);
    }
     
    
    /**
     * 
     * @param Accounts $account
     */
    private function callAccountStatusEvent(Accounts $account)
    {
        $this->eventDispatcher->dispatch(
            ProfileEvents::PROFILE_SET_ACCOUNT_STATUS,
            new ProfileEvent($account)
        );
    }
    
    /**
     * @param Accounts $account
     * @param string   $userAgent
     * @param string   $clientIp
     * @param string   $destination
     * @param string   $locale
     * @param array    $game
     */
    private function callEmailOneStepEvent(Accounts $account, $userAgent, $clientIp, $destination, $locale, $game)
    {
        $this->eventDispatcher->dispatch(
            EmailEvents::SEND_EMAIL_REGISTER_ONE_STEP,
            new EmailRegisterEvent($account, $userAgent, $clientIp, $destination, $locale, $game)
        );
    }
    
    /**
     * @param Accounts     $account
     * @param ParameterBag $cookies
     * @param string       $httpReferer
     * @param string       $userAgent
     */
    private function callCookiesEvent(Accounts $account, ParameterBag $cookies, $httpReferer, $userAgent)
    {
        $this->eventDispatcher->dispatch(
            CookiesEvents::PROCESS_COOKIES_REGISTER,
            new ProcessCookiesEvent($account, $cookies, $httpReferer, $userAgent)
        );
    }
    
    /**
     * 
     * @param Accounts $account
     */
    private function callOnRegisterEvent(Accounts $account)
    {
        $this->eventDispatcher->dispatch(
            QueueEvents::QUEUE_ON_REGISTER_USER,
            new QueueOnRegisterEvent($account)
        );                
    }    
        
    /**
     * 
     * @param Accounts $account
     * @param string $clientIp
     */
    private function callGeoIpEvent(Accounts $account, $clientIp)
    {
        $this->eventDispatcher->dispatch(
            ProfileEvents::PROFILE_SET_COUNTRY,
            new ProfileEvent($account, $clientIp)
        );        
    }
}
