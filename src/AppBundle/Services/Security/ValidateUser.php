<?php

namespace AppBundle\Services\Security;

use Texyon\Database\PortalBundle\Entity\Accounts;

use Symfony\Component\HttpFoundation\ParameterBag;
use Tools\QueueBundle\Event\QueueEvents;
use Tools\QueueBundle\Event\QueueOnValidateEvent;
use Tools\EmailBundle\Event\EmailEvents;
use Tools\CookiesBundle\Event\ProcessCookiesEvent;
use Tools\CookiesBundle\Event\CookiesEvents;
use portal\PublicBundle\Event\HubspotEvent;
use portal\PublicBundle\Event\HubspotEvents;
use Tools\EmailBundle\Event\EmailWelcomeEvent;


/**
 * Class ValidateUser
 *
 * {@inheritdoc}
 */
class ValidateAccount extends Validate
{    
    
    /**
     * 
     * @param Accounts      $account
     * @param string        $authtoken
     * @param string        $ipAddressValidation
     * @param ParameterBag  $cookies
     * @param string        $httpReferer
     * @param string        $userAgent
     * @return Accounts
     */
    public function validate(Accounts $account, $authtoken, $ipAddressValidation, ParameterBag $cookies, $httpReferer, $userAgent, $locale)
    {
        $status = $this->validateAccounts->getActiveStatus();        
        $accountEntity = $this->validateUserOneStep($status, $ipAddressValidation, $account);
        
        $this->accountsRepository->save($accountEntity);
        $this->tokenManager->consumeToken($authtoken);
        //$this->createSession($accountEntity, $ipAddressValidation, $cookies, $httpReferer);

//        $this->callCookiesEvent($accountEntity, $cookies, $httpReferer, $userAgent);
//        $this->callOnValidateEvent($accountEntity);
//        $this->callEventSendMail($account, $locale, $httpReferer);
//        $this->callHubspotEvent( $account, $ipAddressValidation);
        
        return $accountEntity;
    }
    
    /**
     * @param Accounts     $account
     * @param ParameterBag $cookies
     * @param string       $httpReferer
     */
    private function callCookiesEvent(Accounts $account, ParameterBag $cookies, $httpReferer, $userAgent)
    {
        $this->eventDispatcher->dispatch(
            CookiesEvents::PROCESS_COOKIES_VALIDATE,
            new ProcessCookiesEvent($account, $cookies, $httpReferer, $userAgent)
        );
    }

   
    /**
     * 
     * @param Accounts $account
     */
    private function callOnValidateEvent(Accounts $account)
    {
        $this->eventDispatcher->dispatch(
            QueueEvents::QUEUE_ON_VALIDATE_USER,
            new QueueOnValidateEvent($account)
        );
                
    }
    
    /**
     * @param Accounts $account
     * @param string   $locale
     * @param string   $referer
     */
    private function callEventSendMail(Accounts $account, $locale, $referer)
    {

//        $this->eventDispatcher->dispatch(
//            EmailEvents::SEND_EMAIL_TLC_DOWNLOAD,
//            new EmailTlcDownloadEvent($account, $locale, $referer)
//        );

        $this->eventDispatcher->dispatch(
            EmailEvents::SEND_EMAIL_WELCOME,
//            new EmailTlcDownloadEvent($account, $locale, $referer)
            new EmailWelcomeEvent($account, $locale, $referer)
        );
    }
    
    /**
     * 
     * @param AccountsEntity      $account
     * @param string        $clientIp
     */
    private function callHubspotEvent(Accounts $account, $clientIp)
    {
        $this->eventDispatcher->dispatch(
            HubspotEvents::HUBSPOT_VALIDATE_USER,
            new HubspotEvent($account, $clientIp)
        );
    }
    
}
