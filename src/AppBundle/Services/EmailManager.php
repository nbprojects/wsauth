<?php

namespace AppBundle\Services;

use GuzzleHttp\Client;
use AppBundle\Services\Token\TokenManager;
use Texyon\Database\PortalBundle\Entity\Model\Accounts;

/**
 * Description of EmailManager
 *
 * @author NachoBordon
 */
class EmailManager 
{
    /** @var  TokenManager */
    private $tokenManager;
    /** @var Client */    
    private $wsEmailClient;
        
    
    /**
     * 
     * @param TokenManager  $tokenManager
     * @param Client        $wsEmailClient
     */
    public function __construct(
        TokenManager $tokenManager,
        Client $wsEmailClient
    ) {        
        $this->tokenManager = $tokenManager;
        $this->wsEmailClient = $wsEmailClient;
    }
    
    /**
     * 
     * @param Accounts $account
     * @param string   $ipAddress
     * @param string   $locale
     * @param string   $destination
     * @return boolean
     */
    public function sendValidationEmail(Accounts $account, $ipAddress, $locale, $destination)
    {
        $token = $this->tokenManager->createChangePasswordToken($account, $ipAddress);
        $this->sendEmail($account->getEmail(), $locale, $destination, $token);
        
        return true;
    }
    
    /**
     * 
     * @param string $email
     * @param string $locale
     * @param string $destination
     * @param string $token
     * @return type
     */
    private function sendEmail($email, $locale, $destination, $token)
    {
        //$urlPasswordForgot = sprintf('%s?action=rp&token=%s', $referer, $token);
        $urlPasswordForgot = sprintf('%s/%s/#validate:%s', $destination, $locale, $token); // referer = "http://www.texyon.com"

        return $this->wsEmailClient->post('send', ['form_params' => [
                'type' => 'forgot_password',
                'email' => $email,
                'locale' => $locale,
                'data' => ['-token-' => $token, '-url-' => $urlPasswordForgot]
            ]            
        ]); 
    }
    
}
