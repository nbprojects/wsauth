<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Texyon\Database\PortalBundle\Entity\Accounts;


/**
 * Class QueueOnRegisterEvent
 * @package AppBundle\Event
 */
class QueueOnRegisterEvent extends Event
{
     /** @var Accounts */
    private $user;

    /**
     * @param Accounts      $user
     */
    public function __construct(Accounts $user)
    {
        $this->user = $user;
    }

    /**
     * @return Accounts
     */
    public function getUser()
    {
        return $this->user;
    }
}
