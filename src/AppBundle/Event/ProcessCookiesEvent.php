<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Texyon\Database\PortalBundle\Entity\Accounts;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class ProcessCookiesEvent
 * @package AppBundle\Event
 */
class ProcessCookiesEvent extends Event
{
    /** @var Accounts */
    private $user;
    /** @var ParameterBag */
    private $cookies;
    /** @var string */
    private $httpReferer;
    /** @var string */
    private $userAgent;

    /**
     * @param Accounts      $user
     * @param ParameterBag  $cookies
     * @param string        $httpReferer
     * @param string        $userAgent
     */
    public function __construct(
        Accounts $user, 
        ParameterBag $cookies, 
        $httpReferer, 
        $userAgent
    )
    {
        $this->user = $user;
        $this->cookies = $cookies;
        $this->httpReferer = $httpReferer;
        $this->userAgent = $userAgent;
    }

    /**
     * @return Accounts
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * 
     * @return ParameterBag
     */
    function getCookies()
    {
        return $this->cookies;
    }
    
    /**
     * 
     * @return string
     */
    function getHttpReferer()
    {
        return $this->httpReferer;
    }

    /**
     * 
     * @return string
     */
    function getUserAgent()
    {
        return $this->userAgent;
    }
    
}
