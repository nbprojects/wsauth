<?php

namespace AppBundle\Event;

use Texyon\Database\PortalBundle\Entity\Accounts;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class EmailRegisterEvent
 * @package AppBundle\Event
 */
class EmailRegisterEvent extends Event
{
    /** @var Accounts  */
    private $user;
    /** @var string */
    private $userAgent;
    /** @var string */
    private $ipClient;
    /** @var string */
    private $destination;
    /** @var string */
    private $locale;
    /** "var string */
    private $UEIgame;



    /**
     * @param Accounts $user
     * @param boolean  $userAgent
     * @param string   $ipClient
     * @param string   $destination
     * @param string   $locale
     * @param string   $UEIgame
     */
    public function __construct(Accounts $user, $userAgent, $ipClient, $destination, $locale, $UEIgame=null)
    {
        $this->user= $user;
        $this->userAgent = $userAgent;
        $this->ipClient = $ipClient;
        $this->destination = $destination;
        $this->locale = $locale;
        $this->UEIgame = $UEIgame;
    }

    /**
     * @return \Core\CoreDomain\User\User\Accounts
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * 
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }
    
    /**
     * 
     * @return string
     */
    public function getIpClient()
    {
        return $this->ipClient;
    }
    
    /**
     * 
     * @return string
     */
    public function getReferer()
    {
        return $this->referer;
    }
    
    /**
     * 
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }
    
    /**
     * 
     * @return string
     */
    function getUEIgame()
    {
        return $this->UEIgame;
    }

    /**
     * 
     * @return string
     */
    function getDestination() {
        return $this->destination;
    }
}
