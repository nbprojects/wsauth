<?php

namespace AppBundle\Event\Collections;


/**
 * Class CookiesEvents
 * @package AppBundle\Event
 */
class CookiesEvents
{
    const PROCESS_COOKIES_REGISTER = 'process.cookies.on.register';
    const PROCESS_COOKIES_VALIDATE = 'process.cookies.on.validate';
}
