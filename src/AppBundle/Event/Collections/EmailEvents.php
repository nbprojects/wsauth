<?php

namespace AppBundle\Event\Collections;


/**
 * Class EmailEvents
 * @package AppBundle\Event
 */
class EmailEvents
{
    const SEND_EMAIL_REGISTER = 'email.security.on.register';
    const SEND_EMAIL_REGISTER_ONE_STEP = 'email.security.on.register.one.step';
    const SEND_EMAIL_SOCIAL_REGISTER = 'email.security.on.social.register';
    const SEND_EMAIL_ACCOUNT_UNLOCK = 'email.unlock.account';
    const SEND_EMAIL_TLC_DOWNLOAD = 'email.tlc.download';
    const SEND_EMAIL_NOTIFICATION_PORTAL_COUPON = 'email.notification.portal.coupon';
    const SEND_EMAIL_VALIDATION = 'email.validation';
    const SEND_EMAIL_WELCOME = 'email.welcome';
}
