<?php

namespace AppBundle\Event\Collections;

/**
 * Class ProfileEvents
 * @package AppBundle\Event
 */
final class ProfileEvents
{
    const DIRECT_ACTION_CHANGE_PROFILE = 'direct.action.change.profile';
    const PROFILE_SET_COUNTRY = 'profile.set.country';
    const PROFILE_SET_ACCOUNT_STATUS = 'profile.set.account.status';
    
}

