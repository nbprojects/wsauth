<?php

namespace AppBundle\Event\Collections;


/**
 * Class EmailEvents
 * @package AppBundle\Event
 */
class QueueEvents
{
    const QUEUE_ON_REGISTER_USER = 'queue_on_register_user';
    const QUEUE_ON_VALIDATE_USER = 'queue_on_validate_user';
}
