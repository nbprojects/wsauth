<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Texyon\Database\PortalBundle\Entity\Accounts;

/**
 * Class ProfileEvent
 * @package AppBundle\Event
 */
class ProfileEvent extends Event
{
    /** @var Accounts */
    private $user;
    /** @var string */
    private $ipAddress;

    
    /**
     * @param Accounts      $user
     * @param string        $clientIp
     */
    public function __construct(Accounts $user, $clientIp=null)
    {
        $this->user = $user;
        $this->ipAddress = $clientIp;
    }

    /**
     * @return Accounts
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 
     * @return string
     */
    function getIpAddress()
    {
        return $this->ipAddress;
    }
    
}