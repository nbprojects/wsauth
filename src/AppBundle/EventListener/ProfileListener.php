<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Texyon\Database\PortalBundle\Entity\AccountStatus;
use GuzzleHttp\Client;
use AppBundle\Event\ProfileEvent;
use AppBundle\Event\Collections\ProfileEvents; 
use Texyon\Database\PortalBundle\Doctrine\DoctrineDomainManager;
//use portal\PublicBundle\DomainManager\AccountsManager;
//use portal\PublicBundle\DomainManager\AccountStatusManager;
use Texyon\Managers\Lib\Logger;


/**
 * Class CookiesRefererListener
 * @package AppBundle\EventListener
 */
class ProfileListener implements EventSubscriberInterface
{
    /** @var DoctrineDomainManager  */
    protected $domainManager;    
    /** @var Client */
    private $geoipClient;
    /** @var Logger */
    private $logger;

    
    /**
     * @param DoctrineDomainManager     $domainManager     
     * @param Client                    $geoipClient
     * @param Logger                    $logger
     */
    public function __construct(
        DoctrineDomainManager $domainManager,         
        Client $geoipClient,
        Logger $logger
    )
    {
        $this->domainManager = $domainManager;
        $this->geoipClient = $geoipClient;
        $this->logger = $logger;
    }

    /**
     * Returns an array of Event names this subscriber wants to listen to.
     *
     * The array keys are Event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The Event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents()
    {
        return array(
            ProfileEvents::PROFILE_SET_COUNTRY => 'setCountryAction',
            ProfileEvents::PROFILE_SET_ACCOUNT_STATUS => 'setAccountStatusAction'
        );
    }

    /**
     * @param ProfileEvent $event
     */
    public function setCountryAction(ProfileEvent $event)
    {
        $country = $this->getCountry($event->getIpAddress());
        
        if ($country) {
            $account = $event->getUser();
            $account->getProfile()->setCountry($country);
            
            $this->domainManager->save($account);
        }
    }
    
    /**
     * 
     * @param string $ipAdress
     * @return string|null
     */
    private function getCountry($ipAdress)
    {          
        try{            
            $response = $this->geoipClient->get("/", ['query'=>['ip'=>$ipAdress]]);
            $data = json_decode($response->getBody(), true); //$response->json();

            if ($data['status'] === 'success') {
               return $data['code'] ;
            }
             
        }  catch (\Exception $e) {
            $inData = array("geoip_server"=>$this->geoip_server, "ipAdress"=>$ipAdress);
            $this->logger->logException($inData, $e);            
        }
    }
    
    public function setAccountStatusAction(ProfileEvent $event)
    {
        $account = $event->getUser();                
        $code = $this->getStatusCode($account->getStatus()->getId());
        
        $accountStatus = new AccountStatus();
        $accountStatus->newAccountStatus($account, $code);
        
        $this->domainManager->save($accountStatus);
    }
    
    private function getStatusCode($statusId)
    {
        switch ($statusId) {
            case 0:
                return 'VisitorAccountStatus';
            case 1: 
                return 'ValidatedAccountStatus';
            case 2: 
                return 'LockedAccountStatus';
            case 3: 
                return 'ValidatedAccountStatus';
            case 100: 
                return 'BannedAccountStatus';
            case 1001: 
                return 'UnsuscribedAccountStatus';
            case 1101: 
                return 'FakeAccountStatus';
            default:
                return 'VisitorAccountStatus';
        }        
    }
}