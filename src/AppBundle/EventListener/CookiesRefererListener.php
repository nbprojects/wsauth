<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use AppBundle\Event\Collections\CookiesEvents;
use AppBundle\Event\ProcessCookiesEvent;
use AppBundle\Services\Cookies\Cookies;


/**
 * Class CookiesRefererListener
 * @package AppBundle\EventListener
 */
class CookiesRefererListener implements EventSubscriberInterface
{
    /** @var Cookies  */
    private $cookiesManager;

    /**
     * @param CookiesManager $cookiesManager
     */
    public function __construct(Cookies $cookiesManager)
    {
        $this->cookiesManager = $cookiesManager;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents()
    {
        return array(
            CookiesEvents::PROCESS_COOKIES_REGISTER => 'processCookiesRegister',
            CookiesEvents::PROCESS_COOKIES_VALIDATE => 'processCookiesValidate'
        );
    }

    /**
     * @param ProcessCookiesEvent $event
     */
    public function processCookiesRegister(ProcessCookiesEvent $event)
    {        
        $this->cookiesManager->onRegister($event->getUser(), $event->getCookies(), $event->getHttpReferer(), $event->getUserAgent());
    }

    /**
     * @param ProcessCookiesEvent $event
     */
    public function processCookiesValidate(ProcessCookiesEvent $event)
    {        
        $this->cookiesManager->onValidate($event->getUser(), $event->getCookies(), $event->getHttpReferer(), $event->getUserAgent());
    }    
}
