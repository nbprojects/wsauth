<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use AppBundle\Services\Collections\EmailEvents;
use AppBundle\Event\EmailRegisterEvent;
use GuzzleHttp\Client;
use AppBundle\Services\EmailManager;

/**
 * Class SendEmailRegister
 * @package AppBundle\EventListener
 */
class SendEmailRegisterListener implements EventSubscriberInterface
{
    /** @var EmailManager  */
    private $emailManager;

    /**
     * @param Client $emailClient
     */
    public function __construct(EmailManager $emailManager)
    {
        $this->emailManager = $emailManager;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents()
    {
        return array(
            EmailEvents::SEND_EMAIL_REGISTER => 'sendEmail',
            EmailEvents::SEND_EMAIL_REGISTER_ONE_STEP => 'sendEmailOneStep',
            EmailEvents::SEND_EMAIL_VALIDATION => 'sendValidationEmail'
        );
    }

    /**
     * @param EmailRegisterEvent $event
     */
    public function sendEmail(EmailRegisterEvent $event )
    {
    /*    $this->securityEmail->sendRegisterEMail(
                $event->getUser(), 
                $event->getUserAgent(), 
                $event->getIpClient(), 
                $event->getReferer(), 
                $event->getLocale(),
                $event->getUEIgame()
        );
      */  
    }

    /**
     * @param EmailRegisterEvent $event
     */
    public function sendEmailOneStep(EmailRegisterEvent $event )
    {   
        
        $this->emailManager->sendValidationEmail($event->getUser(), $event->getIpClient(), $event->getLocale(), $event->destination);
    }

    /**
     * @param EmailRegisterEvent $event
     */
    public function sendValidationEmail(EmailRegisterEvent $event )
    {
       // $this->securityEmail->sendValidationEmail($event->getUser(), $event->getUserAgent(), $event->getIpClient(), $event->getReferer(), $event->getLocale());

    }
}
