<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use AppBundle\Event\Collections\QueueEvents;
use AppBundle\Event\QueueOnRegisterEvent;
use Texyon\Database\PortalBundle\Entity\Accounts;

use Texyon\Managers\Lib\RabbitManager;

/**
 * Class QueueOnRegisterListener
 * @package AppBundle\EventListener
 */
class QueueOnRegisterListener implements EventSubscriberInterface
{
    /** @var RabbitManager  */
    private $rabbit;

    /**
     * @param RabbitManager $rabbit
     */
    public function __construct(RabbitManager $rabbit)
    {
        $this->rabbit = $rabbit;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents()
    {
        return array(
            QueueEvents::QUEUE_ON_REGISTER_USER => 'onRegister'
        );
    }

    /**
     * @param QueueOnRegisterEvent $event
     */
    public function onRegister(QueueOnRegisterEvent $event)
    {
        $this->addToRegisterQueue($event->getUser());
    }
    
    /**
     * 
     * @param Accounts $user
     */
    private function addToRegisterQueue(Accounts $user)
    {
        $this->rabbit->setQueue('onRegister');
        $this->rabbit->newTask($user->getId());
        
        $this->rabbit->close();
    }

}