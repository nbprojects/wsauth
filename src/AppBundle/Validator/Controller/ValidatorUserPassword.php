<?php
/**
 * Short description file...
 *
 * Long description file (if need)...
 *
 * @package ${VENDOR}\\${BUNDLE}\\$PACKAGE
 * @author  pablo
 * @date    4/12/13 11:37
 */

namespace portal\PublicBundle\Validator\Controller;

use Core\CoreDomain\User\User\Accounts;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class ValidatorUserPassword extends ValidatorController
{
    /** @var  EncoderFactory */
    protected $encoder;

    /**
     * Message
     *
     * @return mixed|string
     */
    public function getMessage()
    {
        return 'validator_invalid_user_password';
    }

    /**
     * Set encoder factory
     *
     * @param EncoderFactory $encoder
     */
    public function setEncoder(EncoderFactory $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * logic validation
     *
     * @return bool|mixed
     */
    public function validate()
    {
        /** @var Accounts $user */
        $user = $this->compareData;
        $encoder = $this->encoder->getEncoder($user);

        $password = $this->getFieldValue();

        return $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());
    }
}
