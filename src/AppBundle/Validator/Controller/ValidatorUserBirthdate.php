<?php
/**
 * Short description file...
 *
 * Long description file (if need)...
 *
 * @package ${VENDOR}\\${BUNDLE}\\$PACKAGE
 * @author  pablo
 * @date    4/12/13 8:49
 */

namespace portal\PublicBundle\Validator\Controller;

use Core\CoreDomain\User\User\Accounts;

class ValidatorUserBirthdate extends ValidatorController
{
    /**
     * Validate user birthdate
     *
     * @return bool
     */
    protected function validate()
    {
        $value = $this->getFieldValue();

        $birthdate = new \DateTime($value['year'].'-'.$value['month'].'-'.$value['day']);

        /** @var Accounts $user */
        $user = $this->compareData;
        $userBirthdate = $user->getProfile()->getBirthDate();

        if ($birthdate->getTimestamp() === $userBirthdate->getTimestamp()) {
            return true;
        }

        return false;
    }

    /**
     * Message.
     *
     * @return mixed|string
     */
    protected function getMessage()
    {
        return 'validator_invalid_user_birthdate';
    }
}
