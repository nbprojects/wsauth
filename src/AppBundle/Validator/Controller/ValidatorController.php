<?php
/**
 * Short description file...
 *
 * Long description file (if need)...
 *
 * @package ${VENDOR}\\${BUNDLE}\\$PACKAGE
 * @author  pablo
 * @date    4/12/13 8:38
 */

namespace portal\PublicBundle\Validator\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Translation\TranslatorInterface;

abstract class ValidatorController
{
    /** @var \Symfony\Component\Form\Form  */
    protected $form;
    /** @var string $fieldName field name */
    protected $fieldName;
    /** @var mixed $compareData Data to compare in validate method. May be object, string, integer, etc. */
    protected $compareData;

    /** @var  TranslatorInterface */
    private $translator;

    /**
     * @param Form   $form
     * @param string $fieldName
     */
    public function __construct(Form $form, $fieldName, $compareData, $translator)
    {
        $this->form = $form;
        $this->fieldName = $fieldName;
        $this->compareData = $compareData;
        $this->translator = $translator;
    }

    /**
     * To apply validator logic.
     *
     * @return mixed
     */
    abstract protected function validate();

    /**
     * To string message.
     *
     * @return mixed
     */
    abstract protected function getMessage();

    /**
     * Check validator.
     *
     * @return mixed
     */
    public function isValid()
    {
        return $this->validate();
    }

    /**
     * Get form field object
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function getField()
    {
        return $this->form->get($this->fieldName);
    }

    /**
     * Get form field value
     *
     * @return mixed
     */
    protected function getFieldValue()
    {
        return $this->getField()->getViewData();
    }

    /**
     * set error message.
     *
     * @return FormError
     */
    protected function setError()
    {
        $message = $this->translator->trans($this->getMessage());

        return new FormError($message);
    }

    /**
     * add error in form and return object.
     *
     * @return $this
     */
    public function setFormError()
    {
        $this->getField()->addError($this->setError());
    }
}
