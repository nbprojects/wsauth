<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use GuzzleHttp\Client;

/**
 * UserExistsValidator Validator
 */
class RecaptchaValidator extends ConstraintValidator
{
    /** @var Client  */
    private $client;
    private $secretKey;
    
    public function __construct(
        Client $clientRecaptcha,
        $secretKey
    ){
        $this->client = $clientRecaptcha;
        $this->secretKey = $secretKey;
    }

    /**
     * Validate method
     *
     * @param string     $value      Value to validate
     * @param Constraint $constraint Constraint parameter
     */
    public function validate($value, Constraint $constraint)
    {
        
        $response = $this->verify($value);
        
        if (!$response['success']) {        
            $message = json_encode($response['error-codes']);
            $this->context->addViolation($message); //$constraint->message
        } //else if && $response['score'] < 0.5)    
    }

    /**
     * return account data.
     *
     * @param  string $recaptcha
     * @return array
     */
    private function verify($recaptcha)
    {             
        $res = $this->client->request('POST', "", ['form_params' => [
            'secret' => $this->secretKey,
            'response' => $recaptcha
            //'remoteip' => $ipAddress
        ]]);
        
        return json_decode($res->getBody(), true);
    }
}
