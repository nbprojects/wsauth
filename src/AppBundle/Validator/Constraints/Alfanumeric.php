<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class Alfanumeric
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class Alfanumeric extends Constraint
{
    public $message = 'validator.alfanumeric.text';
    //public $service = 'validator.unique_nick';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
