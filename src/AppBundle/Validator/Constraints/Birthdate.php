<?php
/**
 * Short description file...
 *
 * Long description file (if need)...
 *
 * @package ${VENDOR}\\${BUNDLE}\\$PACKAGE
 * @author  pablo
 * @date    2/12/13 12:58
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class Birthdate
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class Birthdate extends Constraint
{
    public $message = "validator_invalid_birthdate";

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
