<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class EmailBlackList
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class EmailBlackList extends Constraint
{
    public $message = 'email.domain.blacklist';
    public $service = 'validator.email.blacklist';

    public function validatedBy()
    {
        return $this->service;
    }
}
