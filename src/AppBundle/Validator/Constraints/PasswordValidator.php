<?php
/**
 * Short description file...
 *
 * Long description file (if need)...
 *
 * @package \AppBundle\Validator\Constraints
 * @author  pablo
 * @date    29/11/13 11:22
 */
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use AppBundle\Lib\passwordValidator\PasswordValidator as Validator;

/**
 * PasswordValidator Validator
 */
class PasswordValidator extends ConstraintValidator
{
    /**
     * Validate method
     *
     * @param string     $value      to validate
     * @param Constraint $constraint parameter
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var Validator $validator */
        $validator = new Validator();
        
        
        $strength = 0;

//        if (password.length > 6) $strength += 1 
        if(strlen($value) > 6){ $strength ++; }                
//        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) $strength += 1 
        if($validator->validateLetters($value)){ $strength ++; }
//        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) $strength += 1                 
        if($validator->validateAlphanumeric($value)){ $strength ++; }
//        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) $strength += 1             
        if($validator->validateSpecialCharacters($value)){ $strength ++; }
//        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/)) $strength += 1             
        if($validator->validateSpecialCharactersStrong($value)){ $strength ++; }
          
        
        if ($strength < 2 ) { 
            $this->context->addViolation($constraint->weakMessage);
        }
                
        
       
 /*
        if (false === $validator->validateAlphanumeric($value)) {
            $this->context->addViolation($constraint->alphaMessage);
        }
        
        if (true === $validator->validateIsInDictionary($value)) {
            $this->context->addViolation($constraint->dictionaryMessage);
        }
         * 
         */
    }
}
