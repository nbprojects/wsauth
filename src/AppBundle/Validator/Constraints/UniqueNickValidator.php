<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Texyon\Database\PortalBundle\Entity\Repository\AccountsExtraRepository;

/**
 * UniqueNickValidator Validator
 */
class UniqueNickValidator extends ConstraintValidator
{
    /** @var AccountsExtraRepository */
    private $accountsExtraRepository;

    public function __construct(AccountsExtraRepository $accountsExtraRepository)
    {
        $this->accountsExtraRepository = $accountsExtraRepository;
    }

    /**
     * Validate method
     *
     * @param string     $value      Value to validate
     * @param Constraint $constraint Constraint parameter
     */
    public function validate($value, Constraint $constraint)
    {
         
        if ($this->getAccount($value)) {
            $this->context->addViolation($constraint->message);
        }
    }
    

    /**
     * return account data.
     *
     * @param  string $nick
     * @return ProfileManager
     */
    private function getAccount($nick)
    {
        return $this->accountsExtraRepository->findOneBy(array(
            'uniqueNick' => $nick
        ));
    }

}
