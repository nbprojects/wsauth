<?php
/**
 * Short description file...
 *
 * Long description file (if need)...
 *
 * @package ${VENDOR}\\${BUNDLE}\\$PACKAGE
 * @author  pablo
 * @date    9/12/13 17:50
 */
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class UserExists
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class UserDontExists extends Constraint
{
    public $message = 'validator_user_dont_exists';

    public $service = 'validator.user_dont_exists';

    public function validatedBy()
    {
        return $this->service;
    }
}
