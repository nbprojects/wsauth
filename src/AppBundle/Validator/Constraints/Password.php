<?php
/**
 * Short description file...
 *
 * Long description file (if need)...
 *
 * @package ${VENDOR}\\${BUNDLE}\\$PACKAGE
 * @author  pablo
 * @date    29/11/13 11:24
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class Password
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class Password extends Constraint
{
    public $alphaMessage = 'validator_invalid_alpha_password';
    public $dictionaryMessage = 'validator_invalid_dictionary_password';
    public $weakMessage = 'validator_invalid_weak_password';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
