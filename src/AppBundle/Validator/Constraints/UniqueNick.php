<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class UniqueNick
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class UniqueNick extends Constraint
{
    public $message = 'accounts.nick.exists';
    public $service = 'validator.unique_nick';

    public function validatedBy()
    {
        return $this->service;
        //return get_class($this).'Validator';
    }
}
