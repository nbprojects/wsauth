<?php
/**
 * Short description file...
 *
 * Long description file (if need)...
 *
 * @package \AppBundle\Validator\Constraints
 * @author  pablo
 * @date    9/12/13 17:53
 */
namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Texyon\Database\PortalBundle\Entity\Repository\AccountsRepository;

/**
 * UserExistsValidator Validator
 */
class UserDontExistsValidator extends ConstraintValidator
{
    /** @var AccountsRepository  */
    private $accountsRepository;

    public function __construct(AccountsRepository $accountsRepository)
    {
        $this->accountsRepository = $accountsRepository;
    }

    /**
     * Validate method
     *
     * @param string     $value      Value to validate
     * @param Constraint $Constraint Constraint parameter
     */
    public function validate($value, Constraint $Constraint)
    {
        if (!$this->getAccount($value)) {
            $this->context->addViolation($Constraint->message);
        }
    }

    /**
     * return account data.
     *
     * @param  string  $email
     * @return \Core\CoreDomain\User\User\Accounts
     */
    private function getAccount($email)
    {
        return $this->accountsRepository->findOneBy(array(
                'email' => $email
            )
        );
    }
}
