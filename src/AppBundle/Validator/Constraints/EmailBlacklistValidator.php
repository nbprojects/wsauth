<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Yaml\Parser;
/**
 * Description of EmailBlacklistValidator
 *
 * @author Ignacio
 */
class EmailBlacklistValidator extends ConstraintValidator
{
    
    
    /**
     * Validate method
     *
     * @param string     $value      Value to validate
     * @param Constraint $constraint Constraint parameter
     */
    public function validate($value, Constraint $constraint)
    {
        $domain = $this->getEmailDomain($value);
        
        if (in_array($domain, $this->getBlackList())) {
            $this->context->addViolation($constraint->message);
        }
    }
    
    /**
     * 
     * @return array
     */
    private function getBlackList()
    {
        $yaml = new Parser();
        $blacklist = $yaml->parse(file_get_contents(__DIR__.'/../../../../app/domainBlackList.yml'));
  
        return $blacklist;        
    }

    private function getEmailDomain ( $inthat)
    {
        if (!is_bool(strpos($inthat, '@'))){
            return substr($inthat, strpos($inthat,'@')+strlen('@'));
        }
    }
}
