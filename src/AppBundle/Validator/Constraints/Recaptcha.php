<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class Recaptcha
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class Recaptcha extends Constraint
{
    public $message = 'register.recaptcha';
    public $service = 'validator.recaptcha';

    public function validatedBy()
    {
        return $this->service;
    }
}
