<?php
/**
 * Short description file...
 *
 * Long description file (if need)...
 *
 * @package ${VENDOR}\\${BUNDLE}\\$PACKAGE
 * @author  pablo
 * @date    11/12/13 17:21
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class UserExists
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class UserExists extends Constraint
{
    public $message = 'accounts.email.exists';
    public $service = 'validator.user_exists';

    public function validatedBy()
    {
        return $this->service;
    }
}
