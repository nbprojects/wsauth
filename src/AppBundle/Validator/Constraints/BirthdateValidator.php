<?php
/**
 * Short description file...
 *
 * Long description file (if need)...
 *
 * @package \AppBundle\Validator\Constraints
 * @author  pablo
 * @date    2/12/13 13:01
 */
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * BirthdateValidator Validator
 */
class BirthdateValidator extends ConstraintValidator
{
    /**
     * Validate method
     *
     * @param \DateTime  $value      Value to validate
     * @param Constraint $constraint Constraint parameter
     */
    public function validate($value, Constraint $constraint)
    {
        if (null=== $value) {
            $this->context->addViolation($constraint->message);
        } else {
            $birthdate = $value->getTimestamp();
            $now = new \DateTime();

            if ($now->getTimestamp()<= $birthdate) {
                $this->context->addViolation($constraint->message);
            }
        }

    }
}
