<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\DomainManager\AccountsManager;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * AlfanumericValidator Validator
 */
class AlfanumericValidator extends ConstraintValidator
{

    /**
     * Validate method
     *
     * @param string     $value      Value to validate
     * @param Constraint $constraint Constraint parameter
     */
    public function validate($value, Constraint $constraint)
    {
         
        if (!preg_match('/^[a-zA-Za0-9]+$/', $value, $matches)) {
            //$this->context->addViolation($constraint->message, array('%string%' => $value));
            $this->context->addViolation($constraint->message);
        }
    }
    
}
