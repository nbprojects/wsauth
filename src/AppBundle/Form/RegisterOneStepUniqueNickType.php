<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Router;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


/**
 * RegisterOneStepUniqueNickType form type
 */
class RegisterOneStepUniqueNickType extends AbstractType
{
    /** Router $router */
    private $router;
    
    /**
     * 
     * @param Router $router
     */
    /*public function __construct(Router $router)
    {
        $this->router = $router;
    }*/
    
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $policy =("<a style=\"display: inline\" href=\"http://www.texyon.com/es/policy\">");
        $terms = ("<a style=\"display: inline\" href=\"http://www.texyon.com/es/advise\">");


        $builder
        ->add('uniqueNick', TextType::class, array(
                    'label' => 'profile.label.uniquenickname',
                    'attr' => array(
                        'data-checkurl' => '', //$this->router->generate('api_public_user_uniquenickmd5checker', array(), true),
                        'class' => 'checkUniqueNick',
                        "autocomplete" => "off"
                    ),
                    
                ))
        ->add('email', EmailType::class, array(
                'label' => 'portal_publicbundle_accounts.labels.email',
                'attr' => array('maxlength' =>50,
                                'data-checkurl' => '', //$this->router->generate('api_public_user_emailmd5checker', array(), true),
                                'class' => 'checkEmail',
                                "autocomplete" => "off"
                    ),
            ))
        ->add('password', PasswordType::class,
                array(
                        'label' => 'portal_publicbundle_accounts.labels.password',                        
                        'attr' => array("autocomplete" => "off")
                )
        )
        ->add('profile', '\AppBundle\Form\RegisterProfileType', array(
                'label' => 'portal_publicbundle_accounts.labels.birthdate'
            ))
        ->add('terms', CheckboxType::class, 
            array('label' => 'portal_publicbundle_accounts.labels.legal', 
                //'translation_domain' => 'forms',
                'label_attr' => array("%link_end%" => "</a>", "%terms_link_start%" => $terms,"%privacy_link_start%" => $policy),
            ))
        ->add('recaptcha', TextType::class, array(
                'label' => 'profile.label.recaptcha'
            ))
        ;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'registerOneStepUniqueNick';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Form\Model\RegisterOneStepUniqueNick',
                'translation_domain' => 'forms',
                'csrf_protection' => false
            )
        );
    }

}
