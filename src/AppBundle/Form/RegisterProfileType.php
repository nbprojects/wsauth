<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
 
class RegisterProfileType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {                
        $builder
            ->add('birthDate', BirthdayType::class, array(                
                //'widget' => 'choice',
                //'years' => range(date('Y'), date('Y')-70),
                //'placeholder' => '',
                //'label' => ' ',
                //'data' =>  new \DateTime('01-01-1990')
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('rtsConsent', CheckboxType::class, array(
                    'label' => 'portal_publicbundle_accounts.labels.advertising', 
                    'required' => false                
            ))
        
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Texyon\Database\PortalBundle\Entity\AccountsExtra',
            'translation_domain' => 'forms'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_accountsProfileDate';
    }
}
