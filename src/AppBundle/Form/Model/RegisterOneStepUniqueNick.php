<?php

namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Asserts;
use AppBundle\Validator\Constraints\UserExists;
use AppBundle\Validator\Constraints\EmailBlackList;
use AppBundle\Validator\Constraints\UniqueNick as AssertUniqueNick;
use AppBundle\Validator\Constraints\Alfanumeric;
use AppBundle\Validator\Constraints\Recaptcha as AssertRecaptcha;


/**
 * Class RegisterOneStepUniqueNick
 * 
 * @Asserts\GroupSequence({"RegisterOneStepUniqueNick", "Strict", "Captcha"})
 */
class RegisterOneStepUniqueNick
{
    
    /**
     * @var string
     *
     * @Asserts\NotBlank(message="register.required")
     * @Asserts\Length(min="3", max="25", minMessage="register.uniqueNick.invalid.min", maxMessage="register.uniqueNick.invalid.max", groups={"Strict"})
     * @AssertUniqueNick(groups={"Strict"})
     * @Alfanumeric(groups={"Strict"})
     *
     */
    private $uniqueNick;
    
    /**
     * @var string
     *
     * @Asserts\NotBlank(message="register.required")
     * @Asserts\Length(max="50", groups={"Strict"})
     * @Asserts\Email(checkMX=true, message="register.email.invalid", groups={"Strict"})
     * @UserExists(groups={"Strict"})
     * @EmailBlackList(groups={"Strict"})
     *
     */
    private $email;
    
    /**
     * @var string
     * 
     * @Asserts\NotBlank(message="register.required")
     * 
     */
    private $password;
    
    /**
     *
     * @var string
     * 
     * @Asserts\Valid
     */
    private $profile;

    /**
     * @var string
     *
     * @Asserts\NotBlank(message="register.required")
     * @AssertRecaptcha(groups={"Captcha"})
     *
     */
    private $recaptcha;

    /**
     * @param string $uniqueNick
     */
    public function setUniqueNick($uniqueNick)
    {
        $this->uniqueNick = $uniqueNick;
    }

    /**
     * @return string
     */
    public function getUniqueNick()
    {
        return $this->uniqueNick;
    }
    
    /**
     * @var bool
     */
    private $terms;

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param boolean $terms
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
    }

    /**
     * @return boolean
     */
    public function getTerms()
    {
        return $this->terms;
    }
    
    /**
     * 
     * @return string
     */
    function getPassword()
    {
        return $this->password;
    }

    /**
     * 
     * @return string
     */
    function getProfile()
    {
        return $this->profile;
    }

    /**
     * 
     * @param string $password
     */
    function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * 
     * @param string $profile
     */
    function setProfile($profile)
    {
        $this->profile = $profile;
    }

    function getRecaptcha() {
        return $this->recaptcha;
    }

    function setRecaptcha($recaptcha) {
        $this->recaptcha = $recaptcha;
    }

}
