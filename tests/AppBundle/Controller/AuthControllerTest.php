<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Goutte\Client as GoutteClient;
use GuzzleHttp\Client as GuzzleClient;

class AuthControllerTest extends WebTestCase
{
    private $credentials = [];
        
    public function setUp()
    { 
        $this->credentials = $this->logIn();
    }   
    
    public function testLogin()
    {
        $client = static::createClient();        
        
        $params = [
            'username' => 'user1@texyon.com',
            'password' => '123456789'
        ];
        $crawler = $client->request('GET', '/auth/v1/login', $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('access_token', $responseData);   
    }

    public function testRefreshtoken()
    {
        $client = static::createClient();        
        $params = ['refresh_token' => $this->credentials['refresh_token']];
        $client->request('GET', '/auth/v1/refreshtoken', $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
                
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('access_token', $responseData);   
    }

    public function testValidatetoken()
    {
        $client = static::createClient();        
        $params = ['token' => 'eCDCNKBxdkDTgnitjK'];
        $client->request('GET', '/auth/v1/validate/token', $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
                
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('isValid', $responseData);   
    }

    /*public function testValidateAccount()
    {
        $client = static::createClient();        
        
        $client->request('GET', '/auth/v1/validate/account');
        $this->assertEquals(301, $client->getResponse()->getStatusCode());
                
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('result', $responseData);   
    }*/

    public function testRequestPasswordForgot()
    {
        $client = static::createClient();        
        $params = ['destination' => 'http://localhost', 'email'=>'user1@texyon.com'];
        $client->request('GET', '/auth/v1/request/password/forgot', $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
                
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('result', $responseData);   
    }
    
    public function testRequestPasswordReset()
    {
        $client = static::createClient();        
        $server = [
            "HTTP_AUTHORIZATION" => "Bearer {$this->credentials['access_token']}"                
        ];
        $params = ['destination' => 'http://localhost'];
        $client->request('GET', '/auth/v1/request/password/reset', $params, [], $server);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
                
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('result', $responseData);   
    }
    
    public function testChangeBirthdate()
    {
        $client = static::createClient();        
        $server = [
            "HTTP_AUTHORIZATION" => "Bearer {$this->credentials['access_token']}"                
        ];
        $params = ['destination' => 'http://localhost'];
        $client->request('GET', '/auth/v1/request/change/birthdate', $params, [], $server);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
                
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('result', $responseData);   
    }
    
    private function logIn()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $oauthUrl = $container->getParameter('ws_oauth2_base_uri') . 'token';          
                            
        $queryParams = [
            'client_id' => $container->getParameter('oauth_password.client_id'),
            'client_secret' => $container->getParameter('oauth_password.client_secret'),
            'username' => 'user1@texyon.com',
            'password' => '123456789',
            'grant_type' => 'password',
            'scope' => 'user'
        ];
        
        $guzzleClient = new GuzzleClient(array(
            'base_uri' => $oauthUrl,
            'query'   => $queryParams       
        ));
        
        $goutteClient = new GoutteClient();
        $goutteClient->setClient($guzzleClient);
        $goutteClient->request('GET', $oauthUrl, [], []);
        $responseData = json_decode($goutteClient->getResponse()->getContent(), true);
        $this->assertArrayHasKey('access_token', $responseData);
        
        return $responseData;
    }
}
